**Swiss Army Knife 55v2**

_**Installation**_

_Execute this command list_


Install new laravel project

`composer create-project laravel/laravel [project name] "5.5.*"`


Install swissarmyknife55v2 package

`composer require mmrp/swissarmyknife55v2 dev-master`

Execute 

`php artisan vendor:publish` 

and select 

`Mmrp\Swissarmyknife\SwissArmyKnifeProvider`

Edit file app/routes/api.php and add this

    include('rbac.php')

Execute 

`php artisan migrate`

---

**_Create new crud resource_**

Execute this artisan command

`php artisan make:crud-controller [Namaspace/NameController] --request --model`

    --request: make Crud Request Class
    --model: make Crud Model Class
    
This command create:
* app/Http/Controllers/Namespace/NameController [CrudController]
* app/Http/Requests/Namespace/Name [CrudRequest]
* app/Models/Namespace/Name [CrudModel]
* database/migrations/migration_file [Migration file]
    
Add routes with helper

`createCrudRoute('Namespace\ControllerName')`

Update Actions list with this artisan command

`php artisan rbac:actions`
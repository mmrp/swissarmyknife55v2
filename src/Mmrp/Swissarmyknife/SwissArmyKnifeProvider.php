<?php

namespace Mmrp\Swissarmyknife;

use Illuminate\Support\ServiceProvider;
use Mmrp\Swissarmyknife\Console\Commands\CrudGenerator\ControllerMakeCommand;
use Mmrp\Swissarmyknife\Console\Commands\CrudGenerator\ModelMakeCommand;
use Mmrp\Swissarmyknife\Console\Commands\CrudGenerator\RequestMakeCommand;
use Mmrp\Swissarmyknife\Console\Commands\Rbac\Actions;

class SwissArmyKnifeProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $initDirectory = __DIR__ . '/../../init';

        //Config File
        $this->publishes([
           $initDirectory . '/config/swissarmyknife.php' => base_path('/config/swissarmyknife.php')
        ]);

        //Migrations
        $this->publishes([
            $initDirectory . '/database/migrations' => base_path('database/migrations')
        ]);

        //Routes
        $this->publishes([
            $initDirectory . '/routes/rbac.php' => base_path('/routes/rbac.php')
        ]);

        //RBAC
        $this->publishes([
            $initDirectory . '/Http/Controllers/Api/Rbac' => base_path('app/Http/Controllers/Api/Rbac/'),
            $initDirectory . '/Http/Requests/Api/Rbac' => base_path('app/Http/Requests/Api/Rbac/')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            Actions::class,
            ControllerMakeCommand::class,
            RequestMakeCommand::class,
            ModelMakeCommand::class
        ]);

    }
}

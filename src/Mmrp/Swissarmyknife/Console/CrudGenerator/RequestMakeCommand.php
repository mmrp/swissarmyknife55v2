<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 17/04/18
 * Time: 14:39
 */
namespace Mmrp\Swissarmyknife\Console\Commands\CrudGenerator;

use Illuminate\Console\GeneratorCommand;

class RequestMakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:crud-request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new CRUD request class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Request';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/request.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\Requests';
    }
}
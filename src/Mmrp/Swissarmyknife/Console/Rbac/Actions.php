<?php

namespace Mmrp\Swissarmyknife\Console\Commands\Rbac;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;
use Mmrp\Swissarmyknife\Models\Rbac\Action;
use Mmrp\Swissarmyknife\Models\Rbac\Permission;
use Mmrp\Swissarmyknife\Models\Rbac\Role;

class Actions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbac:actions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update actions list';

    private $routes = [];

    private $actions = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getRoutes();
        $this->refreshActions();
        $this->updatePermissions();
        $this->attachPermissionToRole();
    }

    private function getRoutes()
    {
        $this->routes = Route::getRoutes()->getRoutes();
    }

    private function refreshActions()
    {
        $this->actions =
            array_unique(
                array_values(
                    array_filter(
                        array_map(
                            function($route) {
                                return (
                                    $action = $route->getAction() and
                                    array_key_exists('controller',$action) and
                                    preg_match('/App\\\Http\\\Controllers/',$action['controller'])
                                ) ? str_replace('App\Http\Controllers\\', '',$action['controller']) : NULL;
                            },
                            $this->routes
                        )
                    )
                )
            );

        $insert = array_map( function($action) { return ['name' => $action, 'hash' => sha1($action), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]; }, $this->actions );


        Action::whereNotNull('hash')->delete();
        Action::insert($insert);

        $this->info("Inserted " . count($insert) . " actions");
    }

    private function updatePermissions()
    {
        foreach ($this->actions as $action) {
            $permission = Permission::firstOrNew([ 'name' => $action ]);
            if(!$permission->id) {
                $permission->slug = slug($action);
                $permission->model = $action;
                $permission->save();
                $this->info("Inserted " . $action . " permission");
            }
        }
    }

    private function attachPermissionToRole()
    {
        $permission_id = Permission::all()->pluck('id');

        $role = Role::firstOrNew([ 'name' => 'superuser']);
        if(!$role->id) {
            $role->slug = 'superuser';
            $role->save();
        }
        $role->permissions()->sync($permission_id);
    }
}



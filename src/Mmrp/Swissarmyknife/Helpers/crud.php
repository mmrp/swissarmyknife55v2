<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 20/04/18
 * Time: 11:30
 */


/**
 * @param $controller
 */
function createCrudRoute($controller)
{
    Route::get('/export/logs', $controller . '@exportDataLog');
    Route::get('/export/{type}', $controller . '@exportData');
    Route::get('/export/download/{filePath}', $controller . '@downloadExport');

    Route::get('/bulkinsert/logs', $controller . '@bulkInsertLog');
    Route::get('/bulkinsert/{filePath}/preview', $controller . '@bulkInsertPreview');
    Route::post('/bulkinsert/{filePath}', $controller . '@bulkInsert');

    Route::post('/file/upload', $controller . '@upload');

    Route::get('/', $controller . '@index');
    Route::get('/trash', $controller . '@trash');
    Route::get('/{id}', $controller . '@get');
    Route::post('/store', $controller . '@store');
    Route::put('/multiple/update', $controller . '@multipleUpdate');
    Route::put('/{id?}/update', $controller . '@update');
    Route::delete('/{id}/delete', $controller . '@delete');
    Route::delete( '/{id}/destroy', $controller . '@destroy');
    Route::post('/{id}/restore', $controller . '@restore');
}
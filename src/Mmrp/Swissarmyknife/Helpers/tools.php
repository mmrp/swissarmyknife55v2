<?php

/**
 * @param $value
 * @return mixed
 */
function slug($value)
{
    return str_replace(' ', '_', preg_replace('/[^\d\sa-z]+/i', '', preg_replace("/&([a-z])[a-z]+;/i", "$1", strtolower(htmlentities(html_entity_decode($value))))));
}

/**
 * @param $string
 * @param $bag
 * @param $tagOpen
 * @param $tagClose
 * @return mixed
 */
function paramsReplace($string, $bag = [], $tagOpen = '{{', $tagClose = '}}')
{
    if(is_array($bag) and count($bag)) {
        if(is_array($string)) {
            foreach ($string as $key => $value) {
                $string[$key] = paramsReplace($value, $bag, $tagOpen, $tagClose);
            }
        } else {
            $string = paramsReplaceString($string, $bag, $tagOpen, $tagClose);
        }
    }
    return $string;
}

/**
 * @param $string
 * @param $bag
 * @param $tagOpen
 * @param $tagClose
 * @return mixed
 */
function paramsReplaceString($string, $bag = [], $tagOpen = '{{', $tagClose = '}}')
{
    if(is_array($bag) and count($bag)) {
        $mapping = [];
        if (preg_match_all('/' . $tagOpen . '\s[^{}]+\s' . $tagClose . '/', $string, $matches)) {
            foreach ($matches[0] as $match) {
                $mapping[$match] = $bag[str_replace([$tagOpen . ' ', ' ' . $tagClose], [''], $match)];
            }
        }
        $string = str_replace(array_keys($mapping), array_values($mapping), $string);
    }
    return $string;
}

/**
 * @param $string
 * @param string $tagOpen
 * @param string $tagClose
 * @return string
 */
function evalString($string, $tagOpen = '((', $tagClose = '))')
{
    if (preg_match_all('/' . $tagOpen . '\s[^{}]+\s' . $tagClose . '/', $string, $matches)) {
        $exec = [];

        foreach ($matches[0] as $match) {
            $exec[$match] = eval('return ' . $match . ';');
        }

        $string = implode(array_values($exec));
    }

    return $string;
}

/**
 * @return string
 */
function generateRandomToken()
{
    $crypto_strong = TRUE;

    return sha1(bin2hex(uniqid(openssl_random_pseudo_bytes(256, $crypto_strong),TRUE)));
}

/**
 * @param $url
 * @return mixed|\Mmrp\Swissarmyknife\Packages\Tools\BitLy|null
 */
function bitly($url)
{
    try {
        $bitly = new \Mmrp\Swissarmyknife\Packages\Tools\BitLy($url);
        $bitly = $bitly->get()->url();

        return $bitly;
    }
    catch (\Exception $e) {
        return null;
    }
}

/**
 * @param $sender
 * @param $recipient
 * @param $message
 * @param null $engine [aws-sns|tim-sms-pro|mail-up]
 * @return bool|mixed|string
 */
function sensSMS($sender, $recipient, $message, $engine = NULL)
{
    $engine = is_null($engine) ? config('swissarmyknife.sms_default_provider') : $engine;

    switch ($engine) {
        case 'aws-sns':
            $result = awsSnsSms($sender, $recipient, $message);
            break;
        case 'tim-sms-pro':
            $result = timProSms($sender, $recipient, $message);
            break;
        case 'mail-up':
            $result = mailUpSms($sender, $recipient, $message);
            break;
        default:
            $result = 'Invalid Engine. Try with [aws-sns|tim-sms-pro|mail-up]';
            break;
    }

    return $result;
}
/**
 * @param $sender
 * @param $recipient
 * @param $message
 * @return bool
 * @throws Exception
 */
function awsSnsSms($sender, $recipient, $message)
{
    $snsRequest = new \Mmrp\Swissarmyknife\Packages\Tools\Sms\AwsSns\AwsSnsRequest();
    $snsRequest->region = config('swissarmyknife.aws_sns_region');
    $snsRequest->key = config('swissarmyknife.aws_sns_key');
    $snsRequest->secret = config('swissarmyknife.aws_sns_secret');

    if(!$snsRequest->region or
        !$snsRequest->key or
        !$snsRequest->secret
    ) {
        throw new \Exception('Check if are set aws_sns_region or aws_sns_secret or aws_sns_key ');
    }

    $snsRequest->sender = $sender;
    $snsRequest->recipient = $recipient;
    $snsRequest->message = $message;

    $sns = new \Mmrp\Swissarmyknife\Packages\Tools\Sms\AwsSns\AwsSns($snsRequest);

    return $sns->send()->status();
}

/**
 * @param $sender
 * @param $recipient
 * @param $message
 * @return mixed
 * @throws Exception
 */
function timProSms($sender, $recipient, $message)
{
    $timSmsProRequest = new \Mmrp\Swissarmyknife\Packages\Tools\Sms\TimSmsPro\TimSmsProRequest();
    $timSmsProRequest->username = config('swissarmyknife.tim_sms_pro_username');
    $timSmsProRequest->password = config('swissarmyknife.tim_sms_pro_password');
    $timSmsProRequest->token = config('swissarmyknife.tim_sms_pro_token');

    if(!$timSmsProRequest->username or
        !$timSmsProRequest->password or
        !$timSmsProRequest->token
    ) {
        throw new \Exception('Check if are set tim_sms_pro_username or tim_sms_pro_password or tim_sms_pro_token ');
    }

    $timSmsProRequest->sender = $sender;
    $timSmsProRequest->recipient = $recipient;
    $timSmsProRequest->message = $message;

    $timSmsPro = new \Mmrp\Swissarmyknife\Packages\Tools\Sms\TimSmsPro\TimSmsPro($timSmsProRequest);

    return $timSmsPro->send();
}

/**
 * @param $sender
 * @param $recipient
 * @param $message
 * @return mixed
 * @throws Exception
 */
function mailUpSms($sender, $recipient, $message)
{
    $mailUpRequest = new \Mmrp\Swissarmyknife\Packages\Tools\Sms\MailUp\MailUpRequest();

    $mailUpRequest->accountUsername = config('swissarmyknife.mailup_username');
    $mailUpRequest->accountPassword = config('swissarmyknife.mailup_password');
    $mailUpRequest->listGuid = config('swissarmyknife.mailup_list_guid');
    $mailUpRequest->listSecret = config('swissarmyknife.mailup_list_secret');
    $mailUpRequest->campaignCode = config('swissarmyknife.mailup_campaign_code');
    $mailUpRequest->dynamicFields = config('swissarmyknife.mailup_dynamic_fields');
    $mailUpRequest->isUnicode = config('swissarmyknife.mailup_is_unicode');

    if(!$mailUpRequest->accountUsername or
        !$mailUpRequest->accountPassword or
        !$mailUpRequest->listGuid or
        !$mailUpRequest->listSecret or
        !$mailUpRequest->campaignCode or
        !$mailUpRequest->dynamicFields or
        !$mailUpRequest->isUnicode

    ) {
        throw new \Exception('Check if are set mailup_username or mailup_password or mailup_list_guid or mailup_list_secret or mailup_campaign_code or mailup_dynamic_fields or mailup_is_unicode');
    }

    $mailUpRequest->sender = $sender;
    $mailUpRequest->recipient = $recipient;
    $mailUpRequest->message = $message;

    $mailUp = new \Mmrp\Swissarmyknife\Packages\Tools\Sms\MailUp\MailUp($mailUpRequest);

    return $mailUp->send();
}

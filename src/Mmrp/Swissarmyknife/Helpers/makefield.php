<?php

/**
 * @param \Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model
 * @param string $field
 * @return bool
 */
function isreadable(Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model, $field)
{
    return (in_array($field, $model->getHidden()) ? FALSE : TRUE);
}

/**
 * @param \Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model
 * @param string $field
 * @return bool
 */
function isWritable(Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model, $field)
{
    return $model->isFillable($field);
}

/**
 * @param \Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model
 * @param string $field
 * @param boolean $sortable
 * @return array
 */
function makeFieldInput(Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model, $field, $sortable = TRUE){
    return [
        'fieldName' => $field,
        'dbType' => $model->getConnection()->getDoctrineColumn($model->getTable(),$field)->getType()->getName(),
        'uiType' => 'input',
        'dataDomain' => null,
        'settings' => [
            'sortable' => $sortable,
            'writable' => isWritable($model, $field),
            'readable' => isreadable($model, $field)
        ]
    ];
}

/**
 * @param \Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model
 * @param string $field
 * @return array
 */
function makeFieldHidden(Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model, $field){
    return [
        'fieldName' => $field,
        'dbType' => $model->getConnection()->getDoctrineColumn($model->getTable(),$field)->getType()->getName(),
        'uiType' => 'hidden',
        'dataDomain' => null,
        'settings' => null
    ];
}

/**
 * @param \Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model
 * @param string $field
 * @param bool $confirmed
 * @return array
 */
function makeFieldPassword(Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model, $field, $confirmed = FALSE){
    return [
        'fieldName' => $field,
        'dbType' => $model->getConnection()->getDoctrineColumn($model->getTable(),$field)->getType()->getName(),
        'uiType' => 'password',
        'dataDomain' => null,
        'settings' => [
            'confirmed' => $confirmed,
            'writable' => isWritable($model, $field),
            'readable' => isreadable($model, $field),
        ]
    ];
}

/**
 * @param \Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model
 * @param string $field
 * @param array $domain
 * @param boolean $sortable
 * @return array
 */
function makeFieldSelect(Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model, $field, $domain, $sortable = TRUE){
    return [
        'fieldName' => $field,
        'dbType' => $model->getConnection()->getDoctrineColumn($model->getTable(),$field)->getType()->getName(),
        'uiType' => 'select',
        'dataDomain' => $domain,
        'settings' => [
            'sortable' => $sortable,
            'writable' => isWritable($model, $field),
            'readable' => isreadable($model, $field),
        ]
    ];
}

/**
 * @param \Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model
 * @param $field
 * @return array
 */
function makeFieldProfileImage(Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model, $field)
{
    return [
        'fieldName' => $field,
        'dbType' => $model->getConnection()->getDoctrineColumn($model->getTable(),$field)->getType()->getName(),
        'uiType' => 'inputFile',
        'dataDomain' => null,
        'settings' => null
    ];
}

/**
 * @param \Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model
 * @param string $relation
 * @param string $related
 * @param string $attribute
 * @param string $type [one-to-one | one-to-many | belongs-to | many-to-many ]
 * @param boolean $sortable
 * @return array
 */
function makeFieldRelationship(Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model $model, $relation, $related, $attribute, $type, $sortable = TRUE)
{
    return [
        'relation' => $relation,
        'fieldName' => $related,
        'dbType' => $model->getConnection()->getDoctrineColumn($model->getTable(),$model->getKeyName())->getType()->getName(),
        'uiType' => 'relation',
        'dataDomain' => null,
        'settings' => [
            'readable' => TRUE,
            'writable' => TRUE,
            'attribute' => $attribute,
            'sortable' => $sortable,
            'type' => $type,
        ]
    ];
}

<?php

/**
 * @param $code
 * @return mixed
 */
function http_response($code)
{
    return Symfony\Component\HttpFoundation\Response::$statusTexts[$code];
}

/**
 * @param $json
 * @return bool|mixed
 */
function parseJSON($json)
{
    try {
        $object = json_decode($json);

        if ($object === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception("Invalid JSON", 400);
        }

        return $object;
    }
    catch (\Exception $e) {
        return false;
    }
}
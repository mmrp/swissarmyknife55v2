<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 07/08/18
 * Time: 10:29
 */

namespace Mmrp\Swissarmyknife\Packages\Api\Model;

use Illuminate\Pagination\LengthAwarePaginator;
use Mmrp\Swissarmyknife\Packages\Api\Api;
use Mmrp\Swissarmyknife\Packages\Api\ApiException;
use Mmrp\Swissarmyknife\Packages\Api\ApiRequest;

class ApiModel
{
    private $connection = NULL;

    protected $url;
    protected $resource;

    public $attributes;
    public $settings;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->connection = new \stdClass();
        $this->connection->apiRequest = new ApiRequest();
        $this->connection->apiRequest->name = $this->resource;
        $this->connection->apiRequest->url = $this->url;

        $this->attributes = new \stdClass();
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->attributes->$name;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function __set($name, $value)
    {
        $this->attributes->$name = $value;

        return $this;
    }

    /**
     * @param $header
     * @param $value
     * @return $this
     */
    public function header($header, $value)
    {
        $this->connection->apiRequest->headers[$header] = $value;

        return $this;
    }

    /**
     * @param $headers
     * @return $this
     */
    public function headers($headers)
    {
        foreach ($headers as $header => $value) {
            $this->header($header, $value);
        }

        return $this;
    }

    /**
     * @param array $headers
     */
    public function addCustomHeaders($headers = [])
    {
        $this->connection->apiRequest->addValidHeaders($headers);
    }

    /**
     * @param $field
     * @param $operator
     * @param $value
     * @return $this
     */
    public function where($field, $operator, $value)
    {
        if(!isset($this->connection->apiRequest->query['q'])) {
            $this->connection->apiRequest->query['q'] = '';
        }

        $q = \json_decode($this->connection->apiRequest->query['q']);
        $q[] = [ $field, $operator, $value ];
        $this->connection->apiRequest->query['q'] =  \json_encode($q);

        return $this;
    }

    /**
     * @param $column
     * @param $sort
     * @return $this
     */
    public function orderBy($column, $sort)
    {
        if(!isset($this->connection->apiRequest->query['o'])) {
            $this->connection->apiRequest->query['o'] = '';
        }

        $o = \json_decode($this->connection->apiRequest->query['o']);
        $o[] = [ $column, $sort ];
        $this->connection->apiRequest->query['o'] =  \json_encode($o);

        return $this;
    }

    /**
     * @return \Illuminate\Support\Collection|mixed
     * @throws ApiException
     * @throws \Exception
     */
    public function get()
    {
        try {
            $this->connection->apiRequest->verb = 'GET';

            $this->connection->api = new Api($this->connection->apiRequest);

            return $this->makeCollection($this->send());
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return mixed
     */
    public function first()
    {
        return $this->get()->first();
    }

    /**
     * @return mixed
     */
    public function last()
    {
        return $this->get()->last();
    }

    /**
     * @param int $page
     * @param int $per_page
     * @return LengthAwarePaginator
     * @throws \Exception
     */
    public function paginate($page = 1, $per_page = 15)
    {
        try {
            $this->connection->apiRequest->verb = 'GET';
            $this->connection->apiRequest->query['page'] = $page;
            $this->connection->apiRequest->query['per_page'] = $per_page;

            $this->connection->api = new Api($this->connection->apiRequest);

            return $this->makePaginatedCollection($this->send());
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function find($id)
    {
        try {
            $this->connection->apiRequest->verb = 'GET';

            $this->connection->apiRequest->url .=  $id;

            $this->connection->api = new Api($this->connection->apiRequest);

            $response = $this->send();

            return $this->makeRow($response->payload, $response->settings);
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function trash()
    {
        try {
            $this->connection->apiRequest->verb = 'GET';

            $this->connection->apiRequest->url .= 'trash';

            $this->connection->api = new Api($this->connection->apiRequest);

            return $this->makeCollection($this->send());
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function save()
    {
        try {
            if(isset($this->attributes->id)) {
                $this->connection->apiRequest->verb = 'PUT';
                $this->connection->apiRequest->url .= '/update';
                $this->connection->apiRequest->url = str_replace(
                    '//update',
                    '/' . $this->attributes->id . '/update',
                    $this->connection->apiRequest->url
                );
            } else {
                $this->connection->apiRequest->verb = 'POST';
                $this->connection->apiRequest->url .= 'store';
            }

            $this->connection->apiRequest->data = (array)$this->attributes;

            $this->connection->api = new Api($this->connection->apiRequest);

            $response = $this->send();

            return $this->makeRow($response->payload->data, $response->settings);
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return $this->DelDestrRest('delete');
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function destroy()
    {
        try {
            return $this->DelDestrRest('destroy');
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function restore()
    {
        try {
            return $this->DelDestrRest('restore');
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $action
     * @return \Illuminate\Support\Collection
     */
    private function DelDestrRest($action)
    {
        $verb = $endpoint = $response = NULL;

        switch ($action) {
            case 'delete':
                $verb = 'DELETE';
                $endpoint = '/delete';
                break;
            case 'destroy':
                $verb = 'DELETE';
                $endpoint = '/destroy';
                break;
            case 'restore':
                $verb = 'POST';
                $endpoint = '/restore';
                break;
        }

        if(!isset($this->connection->api)) {
            foreach (($action == 'delete') ? $this->get() : $this->trash() as $item) {
                $rows_id[] = $item->id;
            }
            $this->connection->apiRequest->url .= 'multiple' . $endpoint;
            $this->connection->apiRequest->data = ['rows_id' => $rows_id];
        }
        else if(isset($this->attributes->id)) {
            $this->connection->apiRequest->url .= $endpoint;
            $this->connection->apiRequest->data = NULL;
        }

        $this->connection->apiRequest->url = str_replace('trash','', $this->connection->apiRequest->url);
        $this->connection->apiRequest->verb = $verb;
        $this->connection->api = new Api($this->connection->apiRequest);

        return $this->send();
    }

    /**
     * @return \Illuminate\Support\Collection
     * @throws ApiException
     * @throws \Exception
     */
    private function send()
    {
        try {
            return $this->connection->api->send()->getResponse();
        }
        catch (ApiException $e) {
            throw new ApiException($e->getMessage());
        }

        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        finally {
            $this->connection->debug = $this->connection->api->debug;
        }
    }

    /**
     * @param $response
     * @return \Illuminate\Support\Collection
     */
    private function makeCollection($response)
    {
        return collect($this->makeRows($response->payload, $response->settings));
    }

    /**
     * @param $response
     * @return LengthAwarePaginator
     */
    private function makePaginatedCollection($response)
    {
        $paginator = new LengthAwarePaginator(
            count($response->payload->data),
            $response->payload->total,
            $response->payload->per_page,
            $response->payload->current_page
        );

        $paginator->setCollection(collect($this->makeRows($response->payload->data, $response->settings)));

        return $paginator;
    }

    /**
     * @param $data
     * @param $settings
     * @return array
     */
    private function makeRows($data, $settings)
    {
        $rows = [];

        foreach ($data as $i => $row) {
            $rows[$i] = $this->makeRow($row, $settings);
        }

        return $rows;
    }

    /**
     * @param $settings
     * @param $row
     * @return mixed
     */
    private function makeRow($row, $settings)
    {
        $model = eval('return new ' . get_called_class() . '();');
        $model->settings = $settings;
        $model->connection = $this->connection;

        foreach ($row as $attribute => $value) {
            $model->attributes->$attribute = $value;
        }

        return $model;
    }

}
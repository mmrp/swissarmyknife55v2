<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 07/03/18
 * Time: 17:39
 */

namespace Mmrp\Swissarmyknife\Packages\Api;

/**
 * Class ApiRequest
 * @package App\Packages\Api
 */
class ApiRequest extends ApiValidator
{
    /**
     * @var
     */
    public $name;

    /**
     * @var
     */
    public $url;

    /**
     * @var
     */
    public $verb;

    /**
     * @var
     */
    public $headers;

    /**
     * @var
     */
    public $query;

    /**
     * @var
     */
    public $data;

    /**
     * @var bool
     */
    public $multipart = FALSE;


}
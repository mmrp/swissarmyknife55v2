<?php

/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 07/03/18
 * Time: 17:38
 */

namespace Mmrp\Swissarmyknife\Packages\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Api
{
    /**
     * @var ApiRequest
     */
    protected $apiRequest;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var
     */
    protected $options = [];

    /**
     * @var
     */
    protected $response;

    /**
     * @var ApiDebug
     */
    public $debug;


    /**
     * Api constructor.
     * @param ApiRequest $apiRequest
     */
    public function __construct(ApiRequest $apiRequest)
    {
        $this->apiRequest = $apiRequest;
        $this->client = new Client();
        $this->debug = new ApiDebug($apiRequest);
        $this->options();
    }

    /**
     * @return $this|mixed
     * @throws ApiException
     */
    public function send()
    {
        $this->apiRequest->validate();

        $this->debug->setRequestStartAt(microtime(TRUE));

        try {
            $this->client = $this->client->request(
                $this->apiRequest->verb,
                $this->apiRequest->url,
                $this->options
            );

            $this->response = $this->response();

            return $this;
        }
        catch (RequestException $e) {
            return $this->requestException($e);
        }
        catch (\Exception $e) {
            throw new ApiException($e->getCode() . ' - ' . $e->getMessage());
        }
        finally {
            $this->debug->setRequestEndAt(microtime(TRUE));
        }
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return mixed
     */
    private function response()
    {
        $contentType = $this->client->getHeader('content-type')[0];

        if(preg_match('/application\/json/', $contentType)) {
            return \GuzzleHttp\json_decode($this->client->getBody());
        }

        if(preg_match('/application\/rss\+xml/', $contentType)) {
            return simplexml_load_string($this->client->getBody());
        }

        return $this->client->getBody();
    }

    private function options()
    {
        if($this->apiRequest->headers) {
            $this->options['headers'] = $this->apiRequest->headers;
        }

        if($this->apiRequest->query) {
            $this->options['query'] = $this->apiRequest->query;
        }

        if($this->apiRequest->data and in_array($this->apiRequest->verb, ['POST','DELETE'])) {
            $this->options['body'] = json_encode($this->apiRequest->data);
        }

        if($this->apiRequest->data and $this->apiRequest->verb == 'POST' and $this->apiRequest->multipart == TRUE) {
            foreach ($this->apiRequest->data as $field => $value) {
                $data[] = [
                    'name' => $field,
                    'contents' => $value
                ];
            }

            $this->options['multipart'] = $data;
        }

        if($this->apiRequest->data and $this->apiRequest->verb == 'PUT') {
//            $this->options['form_params'] = $this->apiRequest->data;
            $this->options['body'] = json_encode($this->apiRequest->data);

        }
    }

    /**
     * @param RequestException $e
     * @return mixed
     * @throws ApiException
     */
    private function requestException(RequestException $e)
    {
        switch ($e->getCode()) {
            case 404:
                throw new ApiException('[API:' . $this->apiRequest->verb . '][404] - Not Found! URL:' . $this->apiRequest->url);
                break;

            case 422:
                $this->response = \json_decode($e->getResponse()->getBody());
                break;

            case 503:
                throw new ApiException('[API:' . $this->apiRequest->verb . '][503] - Service Unavailable! URL:' . $this->apiRequest->url);
                break;

            default:
                throw new ApiException( '[API:' . $this->apiRequest->verb . '][' . $e->getCode() . '] - ' . $e->getMessage());
                break;
        }

        return $this;
    }
}
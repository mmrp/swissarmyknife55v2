<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 08/03/18
 * Time: 16:09
 */

namespace Mmrp\Swissarmyknife\Packages\Api;


class ApiDebug
{
    private $request_start_at;
    private $request_end_at;
    private $request_total_time;
    private $apiRequest;

    public function __construct(ApiRequest $apiRequest)
    {
        $this->apiRequest = $apiRequest;
    }

    /**
     * @return mixed
     */
    public function getRequestStartAt()
    {
        return $this->request_start_at;
    }

    /**
     * @return mixed
     */
    public function getRequestEndAt()
    {
        return $this->request_end_at;
    }

    /**
     * @return mixed
     */
    public function getRequestTotalTime()
    {
        return $this->request_total_time;
    }

    /**
     * @param mixed $request_start_at
     */
    public function setRequestStartAt($request_start_at)
    {
        $this->request_start_at = $request_start_at;
    }

    /**
     * @param mixed $request_end_at
     */
    public function setRequestEndAt($request_end_at)
    {
        $this->request_end_at = $request_end_at;
        $this->request_total_time = $this->request_end_at - $this->request_start_at;
    }

    public function toArray()
    {
        $return = [];

        foreach (get_object_vars($this) as $name => $value) {
            $return[$name] = $value;
        }

        return $return;
    }

    public function toObject()
    {
        $return = new \stdClass();

        foreach (get_object_vars($this) as $name => $value) {
            $return->$name = $value;
        }

        return $return;
    }

}
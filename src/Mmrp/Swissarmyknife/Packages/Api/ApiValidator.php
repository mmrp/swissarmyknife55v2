<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 07/03/18
 * Time: 18:03
 */

namespace Mmrp\Swissarmyknife\Packages\Api;


class ApiValidator
{

    const VERB = [
        'GET',
        'POST',
        'PUT',
        'DELETE',
        'HEAD',
        'PATCH'
    ];

    const HEADERS = [
        'Accept',
        'Accept-Charset',
        'Accept-Encoding',
        'Accept-Language',
        'Accept-Datetime',
        'Access-Control-Request-Method',
        'Access-Control-Request-Headers',
        'Auth',
        'Authorization',
        'Cache-Control',
        'Connection',
        'Cookie',
        'Content-Length',
        'Content-MD5',
        'Content-Type',
        'Date',
        'Expect',
        'Forwarded',
        'From',
        'Host',
        'If-Match',
        'If-Modified-Since',
        'If-None-Match',
        'If-Range',
        'If-Unmodified-Since',
        'Max-Forwards',
        'Origin',
        'Pragma',
        'Proxy-Authorization',
        'Range',
        'Referer',
        'TE',
        'User-Agent',
        'Upgrade',
        'Via',
        'Warning',
        'X-Requested-With',
        'DNT',
        'X-Forwarded-For',
        'X-Forwarded-Host',
        'X-Forwarded-Proto',
        'Front-End-Https',
        'X-Http-Method-Override',
        'X-ATT-DeviceId',
        'X-Wap-Profile',
        'Proxy-Connection',
        'X-UIDH',
        'X-Csrf-Token',
        'X-Request-ID',
        'X-Correlation-ID',
        'Response fields',
        'Standard response fields',
        'Access-Control-Allow-Origin',
        'Access-Control-Allow-Credentials',
        'Access-Control-Expose-Headers',
        'Access-Control-Max-Age',
        'Access-Control-Allow-Methods',
        'Access-Control-Allow-Headers',
        'Accept-Patch',
        'Accept-Ranges',
        'Age',
        'Allow',
        'Alt-Svc',
        'Content-Disposition',
        'Content-Encoding',
        'Content-Language',
        'Content-Location',
        'Content-Range',
        'ETag',
        'Expires',
        'Last-Modified',
        'Link',
        'Location',
        'P3P',
        'Proxy-Authenticate',
        'Public-Key-Pins',
        'Retry-After',
        'Server',
        'Set-Cookie',
        'Strict-Transport-Security',
        'Trailer',
        'Transfer-Encoding',
        'Tk',
        'Vary',
        'WWW-Authenticate',
        'X-Frame-Options',
        'Common non-standard response fields',
        'Content-Security-Policy',
        'X-Content-Security-Policy',
        'X-WebKit-CSP',
        'Refresh',
        'Status',
        'Timing-Allow-Origin',
        'Upgrade-Insecure-Requests',
        'X-Content-Duration',
        'X-Content-Type-Options',
        'X-Powered-By',
        'X-UA-Compatible',
        'X-XSS-Protection'
    ];

    private $validHeaders = [];

    public function addValidHeaders($headers)
    {
        $this->validHeaders= $headers;
    }

    public function validate()
    {
        $this->validateName();

        $this->validateVerb();

        $this->validateUrl();

        $this->validateHeaders();
    }

    protected function validateUrl()
    {
        if(empty($this->url)) {
            throw new \Exception( 'url attribute must be valued');
        }

        if(!preg_match('/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/', $this->url))
        {
            throw new \Exception( 'url attribute is not a valid url');
        }

    }

    protected function validateName()
    {
        if (empty($this->name)) {
            throw new \Exception('name attribute must be valued');
        }
    }

    protected function validateVerb()
    {
        if (empty($this->verb)) {
            throw new \Exception('verb attribute must be valued');
        }

        if (!in_array($this->verb,self::VERB)) {
            throw new \Exception('verb attribute is not valid. Check if is uppercase');
        }


    }

    protected function validateHeaders()
    {
        if(!empty($this->headers)) {
            foreach ($this->headers as $header => $values) {
                if (!in_array($header, array_merge(self::HEADERS, $this->validHeaders))) {
                    throw new \Exception($header . ' is not a valid http header');
                }
            }
        }
    }
}
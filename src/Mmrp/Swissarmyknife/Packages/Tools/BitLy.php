<?php

namespace Mmrp\Swissarmyknife\Packages\Tools;
use Mmrp\Swissarmyknife\Packages\Api\Api;
use Mmrp\Swissarmyknife\Packages\Api\ApiRequest;


/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 19/04/18
 * Time: 12:14
 */
class BitLy
{
    private $token;

    private $url;

    private $response;

    /**
     * BitLy constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->token = config('swissarmyknife.bitly_token');
        $this->url = $url;
    }

    /**
     * @return $this|\Exception
     */
    public function get()
    {
        try {
            if(is_null($this->token)) {
                throw new \Exception('Define BITLY_TOKEN env variable');
            }

            $apiRequest = new ApiRequest();
            $apiRequest->name = 'BitLy';
            $apiRequest->verb = 'GET';
            $apiRequest->url = 'https://api-ssl.bitly.com/v3/shorten';
            $apiRequest->query = [
                'access_token' => $this->token,
                'longUrl' => $this->url
            ];

            $api = new Api($apiRequest);

            $this->response = $api->send()->getResponse();

            return $this;
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @return mixed
     */
    public function url()
    {
        return $this->response->data->url;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->response;
    }


}
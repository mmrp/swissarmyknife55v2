<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 21/03/18
 * Time: 15:16
 */

namespace  Mmrp\Swissarmyknife\Packages\Tools\Sms\TimSmsPro;

class TimSmsPro
{

    protected $timSmsProRequest;

    private $url;

    public $debug = [];

    /**
     * MailUp constructor.
     * @param TimSmsProRequest $mailUpRequest
     */
    public function __construct(TimSmsProRequest $timSmsProRequest)
    {
        $this->timSmsProRequest = $timSmsProRequest;

        $this->url = $this->timSmsProRequest->baseUrl;
    }

    public function send()
    {
        $header = [
            'Accept: application/json'
        ];

        $data = [
            'username' => $this->timSmsProRequest->username,
            'password' => $this->timSmsProRequest->password,
            'azienda' => $this->timSmsProRequest->token,
            'alias' => $this->timSmsProRequest->sender,
            'testo' => $this->timSmsProRequest->message,
            'msisdn' => $this->timSmsProRequest->recipient
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        try {
            $result = curl_exec($ch);
            curl_close($ch);

            return $result;
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        finally {

        }

    }
}
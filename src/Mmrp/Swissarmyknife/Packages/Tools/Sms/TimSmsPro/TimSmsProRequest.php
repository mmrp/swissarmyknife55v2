<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 21/03/18
 * Time: 15:16
 */

namespace  Mmrp\Swissarmyknife\Packages\Tools\Sms\TimSmsPro;

class TimSmsProRequest
{
    public $baseUrl = 'https://smartconsole.telecomitalia.it/ssc2-api/rest/send/sms/msisdn/static/immediate/single';

    public $username;

    public $password;

    public $token;

    public $sender;

    public $message;

    public $recipient;

}
<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 21/03/18
 * Time: 15:16
 */

/*
    $mailUpRequest = new MailUpRequest();
    $mailUpRequest->listId = 3;
    $mailUpRequest->listGuid = 'f21cb299-52f4-4d5a-8560-a57303dd5fa5';
    $mailUpRequest->listSecret = '95967010-4ef0-4fd9-b4b5-33f6786f0bd8';

    $mailUpRequest->accountId = '91226';
    $mailUpRequest->accountUsername = 'm91226';
    $mailUpRequest->accountPassword = 'prezzoFiusto!17';

    $mailUpRequest->campaignCode = 'Test_Effe';
    $mailUpRequest->isUnicode = 0;
    $mailUpRequest->dynamicFields = [];
    $mailUpRequest->content = '';

    $mailUpRequest->sender = 'prezzgiusto';
    $mailUpRequest->recipient = '3408351264';
    $mailUpRequest->message = 'Ciao sono effe';

    $mailUp = new MailUp($mailUpRequest);

    dd($mailUp->send());
*/

namespace  Mmrp\Swissarmyknife\Packages\Tools\Sms\MailUp;

use Mmrp\Swissarmyknife\Packages\Api\Api;
use Mmrp\Swissarmyknife\Packages\Api\ApiException;
use Mmrp\Swissarmyknife\Packages\Api\ApiRequest;

class MailUp
{

    protected $mailUpRequest;

    private $url;

    public $debug = [];

    /**
     * MailUp constructor.
     * @param MailUpRequest $mailUpRequest
     */
    public function __construct(MailUpRequest $mailUpRequest)
    {
        $this->mailUpRequest = $mailUpRequest;

        $this->url = $this->mailUpRequest->baseUrl . '/' . $this->mailUpRequest->accountId . '/' . $this->mailUpRequest->listId;
    }

    public function send()
    {

        $apiRequest = new ApiRequest();
        $apiRequest->name = 'MailUp';
        $apiRequest->url = $this->url;
        $apiRequest->verb = 'POST';
        $apiRequest->headers = [
            'Content-Type' => 'application/json;odata=verbose;charset=utf-8',
            'Auth' => [ $this->mailUpRequest->accountUsername, $this->mailUpRequest->accountPassword ]
        ];

        $apiRequest->data = [
            "Content" => $this->mailUpRequest->message,
            "ListGuid" => $this->mailUpRequest->listGuid,
            "ListSecret" => $this->mailUpRequest->listSecret,
            "Recipient" => $this->mailUpRequest->recipient,
            "CampaignCode" => $this->mailUpRequest->campaignCode,
            "DynamicFields" => $this->mailUpRequest->dynamicFields,
            "isUnicode" => $this->mailUpRequest->isUnicode,
            "Sender" => $this->mailUpRequest->sender
        ];

        $api = new Api($apiRequest);

        try {
            $result = $api->send()->getResponse();

            return $result;
        }
        catch (ApiException $e) {
            throw new ApiException($e->getMessage());
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        finally {
            $this->debug = $api->debug;
        }
    }

}
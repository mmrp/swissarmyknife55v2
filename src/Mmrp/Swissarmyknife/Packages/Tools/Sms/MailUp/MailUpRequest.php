<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 21/03/18
 * Time: 15:16
 */

namespace  Mmrp\Swissarmyknife\Packages\Tools\Sms\MailUp;

class MailUpRequest
{
    public $baseUrl = 'https://sendsms.mailup.com/api/v2.0/sms/';

    public $listGuid;

    public $listSecret;

    public $accountId;

    public $accountUsername;

    public $accountPassword;

    public $listId;

    public $message;

    public $sender;

    public $recipient;

    public $campaignCode;

    public $dynamicFields;

    public $isUnicode;

    public $content;


}
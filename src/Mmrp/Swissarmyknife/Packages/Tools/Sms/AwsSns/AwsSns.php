<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 21/03/18
 * Time: 16:30
 */

namespace Mmrp\Swissarmyknife\Packages\Tools\Sms\AwsSns;

use Aws\Sns\SnsClient;
use Aws\Sns\Exception\SnsException;

class AwsSns
{
    private $response;
    protected $awsSnsRequest;

    /**
     * AwsSns constructor.
     * @param AwsSnsRequest $awsSnsRequest
     */
    public function __construct(AwsSnsRequest $awsSnsRequest)
    {
        $this->awsSnsRequest = $awsSnsRequest;
    }

    /**
     * @return $this|\Exception
     */
    public function send()
    {
        try {
            $aws = new SnsClient([
                'version' => 'latest',
                'region' => $this->awsSnsRequest->region,
                'credentials' => [
                    'key' => $this->awsSnsRequest->key,
                    'secret' => $this->awsSnsRequest->secret
                ]
            ]);

            $this->response = $aws->publish([
                'PhoneNumber' => $this->awsSnsRequest->recipient,
                'Message' => $this->awsSnsRequest->message,
                'MessageAttributes' => [
                    'AWS.SNS.SMS.SenderID' => [
                        'DataType' => 'String',
                        'StringValue' => $this->awsSnsRequest->sender
                    ],
                    'AWS.SNS.SMS.SMSType' => [
                        'DataType' => 'String',
                        'StringValue' => 'Transactional',
                    ]
                ],
            ]);

            return $this;
        }
        catch (SnsException $e) {

        }
        catch (\Exception $e) {

        }
    }

    /**
     * @return bool
     */
    public function status()
    {
        $status = false;
        if (isset($this->response['@metadata']['statusCode']) and isset($this->response['@metadata']['statusCode']) == 200) {
            $status =  true;
        }

        return $status;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->response;
    }
}
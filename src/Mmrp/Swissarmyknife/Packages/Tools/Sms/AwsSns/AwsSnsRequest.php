<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 21/03/18
 * Time: 16:30
 */

namespace Mmrp\Swissarmyknife\Packages\Tools\Sms\AwsSns;


class AwsSnsRequest
{
    public $region;

    public $key;

    public $secret;

    public $recipient;

    public $sender;

    public $message;
}
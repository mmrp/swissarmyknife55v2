<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 12/04/18
 * Time: 15:48
 */

namespace Mmrp\Swissarmyknife\Packages\BulkInsert;

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Mmrp\Swissarmyknife\Models\BulkInsert\BulkInsertLog;
use Mmrp\Swissarmyknife\Models\Notification;

class BulkInsert
{
    private $reader;

    private $maxPreviewRows = 100;

    private $filePath;

    private $withHeader = TRUE;
    private $header;
    private $body;

    private $bulkInsertLogId;
    private $bulkInsertLogStoragePath;
    private $bulkInsertLogFileName;

    private $bulkInsertReport = FALSE;

    private $mapping = [];

    /**
     * BulkInsert constructor.
     * @param $filePath
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;

        $this->reader = $this->readerFactory();

        if(get_class($this->reader) == 'Box\Spout\Reader\CSV\Reader') {
            $this->reader->setFieldDelimiter(';');
            $this->reader->setEndOfLineCharacter("\r");
        }

        if($this->withHeader) {
            $this->header = $this->setHeader();
        }
    }


    /**
     * @param int $rows
     * @return $this|\Exception
     */
    public function preview($rows=3)
    {
        try {
            $rowCount = 0;
            $this->reader->open($this->filePath);

            $rows = ($rows > $this->maxPreviewRows) ? $this->maxPreviewRows : $rows;

            foreach ($this->reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    //Skip header if withHeader is TRUE
                    if ($this->withHeader and $rowCount == 0) {
                        $rowCount++;
                        $rows++;
                        continue;
                    }

                    if ($this->withHeader) {
                        $row = $this->assoc($row);
                    }

                    $this->body[] = $row;
                    $rowCount++;

                    //Exit if rows != NULL and rowCount == rows
                    if (!is_null($rows) and $rowCount == $rows) {
                        break;
                    }
                }
            }

            $this->reader->close();

            return $this;
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param Model $model
     * @param bool $bulkInsertReport
     * @return array
     */
    public function bulkInsert(Model $model, $bulkInsertReport = FALSE)
    {
        try {
            if($bulkInsertReport) {
                $this->bulkInsertReport();
            }

            $this->bulkInsertLogId = BulkInsertLog::started($this->filePath, get_class($model));

            $rowCount = 0;
            $this->reader->open($this->filePath);

            DB::transaction(function () use ($model, &$rowCount, $bulkInsertReport) {
                foreach ($this->reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $row) {
                        //Skip header if withHeader is TRUE
                        if ($this->withHeader and $rowCount == 0) {
                            if($bulkInsertReport) {
                                $header = ($this->mapping) ? array_keys($this->mapping) : $this->header;
                                $header[] = 'status';
                                $this->bulkInsertReport->addRow($header);
                            }

                            $rowCount++;
                            continue;
                        }

                        if ($this->withHeader) {
                            $row = $this->assoc($row);
                        }

                        if (count($this->mapping)) {
                            $data = [];
                            foreach (array_keys($this->mapping) as $key) {
                                $data[$key] = $this->map($row, $this->mapping[$key]);
                            }
                        } else {
                            $data = $row;
                        }

                        $rowCount++;

                        try {
                            if(array_key_exists($model->getKeyName(), $data)) {
                                $model->where($model->getKeyName(), $data[$model->getKeyName()])->update($data);
                                $data['status'] = 'updated';
                            } else {
                                $model->insert($data);
                                $data['status'] = 'inserted';
                            }
                        }
                        catch (QueryException $e) {
                            $data['status'] = $e->errorInfo[2];
                        }

                        BulkInsertLog::importing($this->bulkInsertLogId, 'Added ' . $rowCount . ' Rows');

                        if($bulkInsertReport) {
                            $this->bulkInsertReport->addRow(array_values($data));
                        }
                    }
                }
            });

            $this->reader->close();

            BulkInsertLog::ended($this->bulkInsertLogId);
            Notification::success('System','User email', get_class($model), 'Bulk Insert Completed. Added ' . $rowCount . ' Rows', $this->bulkInsertLogFileName);

            if($bulkInsertReport) {
                $this->bulkInsertReport->close();
            }

            return [
                'inserted' => TRUE,
                'rows' => $rowCount
            ];
        }
        catch (\Exception $e) {
            BulkInsertLog::error($this->bulkInsertLogId, $e->getMessage());
            Notification::error('System', 'User', 'Export Failed', $e->getMessage());

            return [
                'inserted' => FALSE,
                'rows' => 0,
                'error' => $e
            ];
        }
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param mixed $mapping
     */
    public function setMapping($mapping)
    {
        $this->mapping = $mapping;
    }

    /**
     * @return array|\Exception
     */
    private function setHeader()
    {
        try {
            $header = [];
            $this->reader->open($this->filePath);

            foreach ($this->reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    $header = $row;

                    break;

                }
            }

            $this->reader->close();

            return $header;
        }
        catch (\Exception $e) {
            return $e;
        }
    }


    /**
     * @return \Box\Spout\Reader\ReaderInterface|\Exception
     */
    private function readerFactory()
    {
        try {
            switch (pathinfo($this->filePath, PATHINFO_EXTENSION)) {
                case 'xlsx':
                    $readerType = Type::XLSX; // for XLSX files
                    break;
                case 'csv':
                    $readerType = Type::CSV; // for CSV files
                    break;
                case 'ods':
                    $readerType = Type::ODS; // for ODS files
                    break;

                default:
                    $readerType = NULL;

            }

            return ReaderFactory::create($readerType);
        }
        catch (\Exception $e) {
            return $e;
        }

    }

    /**
     * @param $row
     * @return array|\Exception
     */
    private function assoc($row)
    {
        try {
            $_tmp = $row;
            $row = [];
            foreach ($_tmp as $key => $value) {
                $row[$this->header[$key]] = $value;
            }
            return $row;
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param $row
     * @param $map
     * @return string
     */
    private function map($row, $map)
    {
        return evalString(paramsReplace(implode($map), $row));
    }

    /**
     *
     */
    private function bulkInsertReport()
    {
        $this->bulkInsertReport = WriterFactory::create(Type::XLSX);
        $this->bulkInsertLogStoragePath = storage_path('app/bulk-insert/report');
        $this->bulkInsertLogFileName = $this->bulkInsertLogStoragePath . '/' . uniqid() . '.xlsx';

        if(!File::isDirectory($this->bulkInsertLogStoragePath)) {
            Storage::makeDirectory('bulk-insert/report');
        }

        $this->bulkInsertReport->openToFile($this->bulkInsertLogFileName);
    }


}
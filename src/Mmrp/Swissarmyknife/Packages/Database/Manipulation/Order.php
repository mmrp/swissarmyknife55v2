<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 14/03/18
 * Time: 17:36
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Manipulation;

class Order
{
    private $order;

    public function __construct($order)
    {

        if(is_object($order)){
            $this->order = $order;
        }

        if(is_array($order)) {
            $this->order = (object) $order;
        }

        if($json = parseJSON($order)) {
            $this->order = $json;
        }
    }

    public function get()
    {
        return $this->order;
    }

    public function toArray()
    {
        return (array) $this->order;
    }

    public function toJson()
    {
        return  json_encode($this->order);
    }
}
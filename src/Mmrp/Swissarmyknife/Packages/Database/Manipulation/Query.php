<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 14/03/18
 * Time: 17:35
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Manipulation;

class Query
{
    private $query;

    public function __construct($query)
    {
        if(is_object($query)){
            $this->query = $query;
        }

        if(is_array($query)) {
            $this->query = (object) $query;
        }

        if($json = parseJSON($query)) {

            $this->query = $json;
        }

    }

    public function get()
    {
        return $this->query;
    }

    public function toArray()
    {
        return (array)$this->query;
    }

    public function toJson()
    {
        return  json_encode($this->query);
    }

    public function collection()
    {
        $collection = [];
        foreach ($this->toArray() as $filter) {
            if(is_string($filter[0])) {
                $object = new \stdClass();
                [$object->column, $object->operator, $object->value, $object->boolean] = $this->parseFilter($filter);

                $collection[] = $object;
            } else {
                //TODO Ricorsione
                $subquery = [];
                foreach ($filter as $item) {
                    $object = new \stdClass();
                    [$object->column, $object->operator, $object->value, $object->boolean] = $this->parseFilter($item);

                    $subquery[] = $object;
                }

                $collection[] = $subquery;
            }
        }

        return collect($collection);
    }


    /**
     * @param $filter
     * @return mixed
     */
    protected function parseFilter($filter)
    {
        //["column","whereNull"]
        if (count($filter) == 2 and $filter[1] == 'whereNull') {
            $filter[2] = null;
            $filter[3] = 'and';
        }

        //["column","whereNull","or"]
        if (count($filter) == 3 and $filter[1] == 'whereNull' and $filter[2] == "or") {
            $filter[2] = null;
            $filter[3] = 'or';
        }

        (count($filter) == 3) ? array_push($filter, 'and') : $filter;

        return $filter;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 04/04/18
 * Time: 14:38
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Manipulation\Traits;

use Carbon\Carbon;

trait TimestampMutators
{
    protected $timezone = 'Europe/Rome';

    public function getCreatedAtAttribute($value)
    {
        return (!is_null($value)) ? Carbon::parse($value)->timezone($this->timezone)->toDateTimeString() : null;
    }

    public function getUpdatedAtAttribute($value)
    {
        return (!is_null($value)) ? Carbon::parse($value)->timezone($this->timezone)->toDateTimeString() : null;
    }

    public function getDeletedAtAttribute($value)
    {
        return (!is_null($value)) ? Carbon::parse($value)->timezone($this->timezone)->toDateTimeString() : null;
    }

    public function getCompletedAtAttribute($value)
    {
        return (!is_null($value)) ? Carbon::parse($value)->timezone($this->timezone)->toDateTimeString() : null;
    }

    public function getNotifyAtAttribute($value)
    {
        return (!is_null($value)) ? Carbon::parse($value)->timezone($this->timezone)->toDateTimeString() : null;
    }

    public function getOpenedAtAttribute($value)
    {
        return (!is_null($value)) ? Carbon::parse($value)->timezone($this->timezone)->toDateTimeString() : null;
    }
}
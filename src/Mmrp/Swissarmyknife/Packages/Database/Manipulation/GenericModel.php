<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 16/03/18
 * Time: 16:25
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Manipulation;

class GenericModel extends Model
{
    protected $table;

    /**
     * Orm constructor.
     * @param array $table
     */
    public function __construct($table)
    {
        $this->table = $table;
        $this->setTable($table);
    }
}
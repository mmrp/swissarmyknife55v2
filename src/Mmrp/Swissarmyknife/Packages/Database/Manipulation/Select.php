<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 14/03/18
 * Time: 17:35
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Manipulation;

class Select
{
    private $select;

    public function __construct($select)
    {

        if(is_object($select)){
            $this->select = $select;
        }

        else if(is_array($select)) {
            $this->select = (object)$select;
        }

        else if($json = parseJSON($select)) {
            $this->select = $json;
        }

    }

    public function get()
    {
        return $this->select;
    }

    public function toArray()
    {
        return (array) $this->select;
    }

    public function toJson()
    {
        return  json_encode($this->select);
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 04/04/18
 * Time: 14:27
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Manipulation;

use Mmrp\Swissarmyknife\Packages\Database\Manipulation\Traits\TimestampMutators;

abstract class Model extends \Illuminate\Database\Eloquent\Model
{
    use TimestampMutators;

    protected $builder;

    protected $project;
    protected $query;
    protected $order;

    /**
     * @param $select
     * @param $query
     * @param $order
     * @return mixed
     */
    public function processQuery($select, $query, $order)
    {
        $this->project = (new Select($select))->toArray();
        $this->query = (new Query($query, $this))->collection();
        $this->order = (new Order($order))->toArray();

        $this->project();
        $this->filter();
        $this->order();

        return $this->builder;
    }

    /**
     *
     */
    protected function project()
    {
        $this->builder = $this;
        if ($this->project and $this->project != ["*"]) {

            $this->builder = $this->select($this->project);
        }
    }

    /**
     *
     */
    protected function filter()
    {
        foreach ($this->query as $filter) {
            if(is_array($filter)) {
                $this->builder = $this->builder->where(function ($query) use($filter){
                    foreach ($filter as $item) {
                        $builder = $this->builder($item);
                        $query->{$builder->function}(...$builder->params);
                    }
                });
            }

            if(is_object($filter)) {
                if(preg_match('/([a-zA-Z]+)\.([a-zA-Z]+)/', $filter->column, $matches)) {
                    $relation = $matches[1];
                    $filter->column = $matches[2];
                    $this->builder = $this->builder->whereHas($relation, function($query) use($filter) {
                        $builder = $this->builder($filter);
                        $query->{$builder->function}(...$builder->params);
                    });
                } else {
                    $builder = $this->builder($filter);
                    $this->builder = $this->builder->{$builder->function}(...$builder->params);
                }
            }
        }
    }

    /**
     * @param $filter
     * @return mixed
     */
    protected function builder($filter)
    {
        $builder = new \stdClass();

        switch ($filter->operator) {
            case '=' :
            case '!=':
            case '<>':
            case '<':
            case '<=':
            case '>':
            case '>=':
                $builder->function = 'where';
                $builder->params = [$filter->column, $filter->operator, $filter->value, $filter->boolean];
                break;

            case 'null':
                $builder->function = 'whereNull';
                $builder->params = [$filter->column, $filter->boolean];
                break;
            case 'not-null':
                $builder->function = 'whereNotNull';
                $builder->params = [$filter->column, $filter->boolean];
                break;

            case 'like':
                if (is_array($filter->value)) {
                    $filter->value = array_values(array_sort($filter->value, function ($array) {
                        return $array;
                    }));
                    $filter->value = implode('%', $filter->value);
                }

                $filter->value = '%' . $filter->value . '%';

                $builder->function = 'where';
                $builder->params = [$filter->column, 'like', $filter->value, $filter->boolean];
                break;
            case 'not-like':
                if (is_array($filter->value)) {
                    $filter->value = array_values(array_sort($filter->value, function ($array) {
                        return $array;
                    }));
                    $filter->value = implode('%', $filter->value);
                }

                $filter->value = '%' . $filter->value . '%';

                $builder->function = 'where';
                $builder->params = [$filter->column, 'not like', $filter->value, $filter->boolean];
                break;

            case 'in':
                $builder->function = 'whereIn';
                $builder->params = [$filter->column, $filter->value, $filter->boolean];
                break;
            case 'not-in':
                $builder->function = 'whereNotIn';
                $builder->params = [$filter->column, $filter->value, $filter->boolean];
                break;

            case 'between':
                $builder->function = 'whereBetween';
                $builder->params = [$filter->column, $filter->value, $filter->boolean];
                break;
            case 'not-between':
                $builder->function = 'whereNotBetween';
                $builder->params = [$filter->column, $filter->value, $filter->boolean];
                break;
        }

        return $builder;
    }

    /**
     *
     */
    protected function order()
    {
        foreach ($this->order as $order) {
            list($field, $type) = $order;

            if(preg_match('/([a-zA-Z]+)\.([a-zA-Z]+)/', $field, $matches)) {
                $relation = $matches[1];
                $field = $matches[2];
                $this->builder = $this->builder->whereHas($relation, function($query) use($field, $type) {
                    $query->orderBy($field, $type);
                });
            } else {
                $this->builder = $this->builder->orderBy($field, $type);
            }
        }
    }

    /**
     * @return array
     */
    public function getColumnsType()
    {
        $fieldsType = [];
        foreach ($this->getFillable() as $item) {
            $fieldsType[$item] = $this->getConnection()->getDoctrineColumn('users','name')->getType()->getName();
        }

        return $fieldsType;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        if(is_object($data)){
            $data = (array) $data;
        }

        else if(is_array($data)) {

        }

        else if($json = $this->parseJSON($data)) {
            $data = $json;
        }

        foreach ($data as $field => $value) {
            $this->$field = $value;
        }

        return $this;
    }
}
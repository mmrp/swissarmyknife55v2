<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 12/03/18
 * Time: 12:19
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Builder;

class ColumnRequest
{
    public $name;

    public $type;

    public $length;

    public $places;

    public $autoincrement;

    public $unsigned;

    public $nullable;

    public $unique;

    public function __construct($column)
    {
        $this->name = $column->name;

        $this->type = $column->type;

        $this->nullable = $this->setNullable($column);

        $this->length = $this->setLength($column);

        $this->places = $this->setPlaces($column);

        $this->autoincrement = $this->setAutoincrement($column);

        $this->unsigned = $this->setUnsigned($column);

        $this->nullable = $this->setNullable($column);

        $this->unique = $this->setUnique($column);
    }

    private function setLength($column)
    {
        if(isset($column->length)) {
            $this->length = $column->length;
        } else {
            if (in_array($column->type, ['decimal', 'float'])) {
                $this->length = 8;
            }

            if (in_array($column->type, ['string', 'char'])) {
                $this->length = NULL;
            }
        }

        return $this->length;
    }

    private function setPlaces($column)
    {
        if(isset($column->places)) {
            $this->places = $column->places;
        } else {
            if (in_array($column->type, ['decimal', 'float'])) {
                $this->places = 2;
            }
        }

        return $this->places;
    }

    private function setAutoincrement($column)
    {
        if(isset($column->autoincrement)) {
            $this->autoincrement = $column->autoincrement;
        } else {
            if ($column->type == 'integer') {
                $this->autoincrement = FALSE;
            } else {
                $this->autoincrement = NULL;
            }
        }

        return $this->autoincrement;
    }

    private function setUnsigned($column)
    {
        if(isset($column->unsigned)) {
            $this->unsigned = $column->unsigned;
        } else {
            if ($column->type == 'integer') {
                $this->unsigned = FALSE;
            } else {
                $this->unsigned = NULL;
            }
        }

        return $this->unsigned;
    }

    private function setNullable($column)
    {
        return isset($column->nullable) ? TRUE : FALSE;
    }

    private function setUnique($column)
    {
        return isset($column->unique) ? TRUE : FALSE;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 12/03/18
 * Time: 14:40
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Builder;

use Illuminate\Database\Schema\Blueprint;

class Constraint
{

    private $table;

    private $constraintRequest;

    public function __construct(Blueprint $table,ConstraintRequest $constraintRequest)
    {
        $this->table = $table;
        $this->constraintRequest = $constraintRequest;
    }

    public function create()
    {
        $constraint = null;

        switch ($this->constraintRequest->type) {
            case 'reference':
                $constraint = $this->reference();
                break;
            case 'unique':
                $constraint = $this->table->unique($this->constraintRequest->columns);
                break;
            case 'index':
                $constraint = $this->table->index($this->constraintRequest->columns);
        }

        return $constraint;
    }

    private function reference()
    {
        $constraint = $this->table
            ->foreign($this->constraintRequest->columns)
            ->references($this->constraintRequest->referenceKey)
            ->on($this->constraintRequest->referenceTable);

        if ($this->constraintRequest->onUpdate) {
            $constraint = $constraint->onUpdate($this->constraintRequest->onUpdate);
        }

        if ($this->constraintRequest->onDelete) {
            $constraint = $constraint->onDelete($this->constraintRequest->onDelete);
        }

        return $constraint;
    }

    public static function debug($type, $columns)
    {
        return [
            'type' => $type,
            'columns' => $columns
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 12/03/18
 * Time: 11:41
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Builder;

use Illuminate\Database\Schema\Blueprint;

class Column
{
    private $columnRequest;

    private $table;

    public function __construct(Blueprint $table, ColumnRequest $columnRequest)
    {
        $this->columnRequest = $columnRequest;
        $this->table = $table;
    }

    public function create()
    {
        $column = NULL;

        switch ($this->columnRequest->type) {
            case 'string':
                $column = $this->table->string($this->columnRequest->name, $this->columnRequest->length);
                break;

            case 'char':
                $column = $this->table->char($this->columnRequest->name, $this->columnRequest->length );
                break;
            case 'text':
                $column = $this->table->text($this->columnRequest->name);
                break;
            case 'longtext':
                $column = $this->table->longText($this->columnRequest->name);
                break;

            case 'increments':
                $column = $this->table->increments($this->columnRequest->name);
                break;
            case 'integer':
                $column = $this->table->integer($this->columnRequest->name, $this->columnRequest->autoincrement, $this->columnRequest->unsigned);
                break;
            case 'float':
                $column = $this->table->float($this->columnRequest->name, $this->columnRequest->length, $this->columnRequest->length);
                break;
            case 'decimal':
                $column = $this->table->decimal($this->columnRequest->name, $this->columnRequest->length, $this->columnRequest->length);
                break;

            case 'boolean':
                $column = $this->table->boolean($this->columnRequest->name);
                break;



            case 'binary':
                $column = $this->table->binary($this->columnRequest->name);
                break;

            case 'datetime':
                $column = $this->table->dateTime($this->columnRequest->name);
                break;
            case 'date':
                $column = $this->table->date($this->columnRequest->name);
                break;
            case 'time':
                $column = $this->table->time($this->columnRequest->name);
                break;
            case 'timestamp':
                $column = $this->table->timestamp($this->columnRequest->name);
                break;
            case 'timestamps':
                $column = $this->table->timestamps();
                break;
        }

        if($this->columnRequest->nullable) {
            $column = $column->nullable();
        }

        if($this->columnRequest->unique) {
            $column = $column->unique();
        }

        return $column;
    }

    public static function debug($action, $column)
    {
        return [
            'action' => $action,
            'column' => $column,
        ];
    }

}
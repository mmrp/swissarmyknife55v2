<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 12/03/18
 * Time: 11:17
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Builder;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Table
{

    private $name;
    private $columns;
    private $constraints = [];
    private $debug = [];

    private $existingColumns;
    private $toAdd;
    private $toDelete;

    public function exec()
    {
        //TODO: determinare properties per creare o meno il file di migration?!


        if(self::exists($this->name)) {
//            $this->existingColumns = Schema::getColumnListing($this->name);
//            $this->toAdd = array_diff(array_pluck($this->columns, 'name'), $this->existingColumns);
//            $this->toDelete = array_diff($this->existingColumns, array_pluck($this->columns, 'name'));
//
//            $this->alter();
        } else {
            $this->create();
        }
    }

    /**
     * @param $table
     * @return mixed
     */
    public static function exists($table)
    {
        return Schema::hasTable($table);
    }

    private function create()
    {
        $this->debug['table']['info'] = [ 'name' => $this->name, 'action' => 'create' ];

        Schema::create($this->name, function (Blueprint $table) {
            foreach ($this->columns as $column) {
                $columnRequest = new ColumnRequest($column);

                (new Column($table, $columnRequest))->create();

                $this->debug['table']['columns'][] = Column::debug('add', $columnRequest->name);
            }

            foreach ($this->constraints as $constraint) {
                $constraintRequest = new ConstraintRequest($constraint);

                (new Constraint($table, $constraintRequest))->create();

                $this->debug['table']['constraints'][] = Constraint::debug($constraintRequest->type, $constraintRequest->columns);
            }
        });
    }

    private function alter()
    {
        $this->debug['table']['info'] = [ 'name' => $this->name, 'action' => 'alter' ];

        Schema::table($this->name, function (Blueprint $table) {
            //add columns
            foreach ($this->columns as $column) {
                if(in_array($column->name, $this->toAdd)) {
                    $columnRequest = new ColumnRequest($column);

                    (new Column($table, $columnRequest))->create();

                    $this->debug['table']['columns'][] = Column::debug('add', $columnRequest->name);
                }
            }

            //remove columns
            foreach ($this->toDelete as $item) {
                $table->dropColumn($item);

                $this->debug['table']['columns'][] = Column::debug('delete', $item);
            };
        });
    }

    /**
     * @return mixed
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @return mixed
     */
    public function getConstraints()
    {
        return $this->constraints;
    }

    /**
     * @return array
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @param mixed $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }

    /**
     * @param mixed $constraints
     */
    public function setConstraints($constraints)
    {
        $this->constraints = $constraints;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
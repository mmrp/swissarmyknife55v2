<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 12/03/18
 * Time: 14:40
 */

namespace Mmrp\Swissarmyknife\Packages\Database\Builder;

class ConstraintRequest
{
    public $type;

    public $columns;

    public $referenceKey;

    public $referenceTable;

    public $onUpdate;

    public $onDelete;


    public function __construct($constraint)
    {
        $this->type = $constraint->type;

        $this->columns = $this->setColumns($constraint);

        $this->referenceKey = $this->setReferenceKey($constraint);

        $this->referenceTable = $this->setReferenceTable($constraint);

        $this->onUpdate = $this->setOnUpdate($constraint);

        $this->onDelete = $this->setOnDelete($constraint);
    }

    private function setColumns($constraint)
    {
        if(is_array($constraint->columns) and count($constraint->columns) == 1) {
            $this->columns = $constraint->columns[0];
        } else {
            $this->columns = $constraint->columns;
        }

        return $this->columns;
    }

    private function setReferenceKey($constraint)
    {
        $this->referenceKey = null;

        if(isset($constraint->referenceKey)) {
            $this->referenceKey = $constraint->referenceKey;
        }

        return $this->referenceKey;
    }

    private function setReferenceTable($constraint)
    {
        $this->referenceTable = null;

        if(isset($constraint->referenceTable)) {
            $this->referenceTable = $constraint->referenceTable;
        }

        return $this->referenceTable;
    }

    private function setOnUpdate($constraint)
    {
        $this->onUpdate = null;

        if(isset($constraint->onUpdate)) {
            $this->onUpdate = $constraint->onUpdate;
        }

        return $this->onUpdate;
    }

    private function setOnDelete($constraint)
    {
        $this->onDelete = null;

        if(isset($constraint->onDelete)) {
            $this->onDelete = $constraint->onDelete;
        }

        return $this->onDelete;
    }

}
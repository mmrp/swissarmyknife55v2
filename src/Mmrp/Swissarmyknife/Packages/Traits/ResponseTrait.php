<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 31/01/18
 * Time: 15:27
 */

namespace Mmrp\Swissarmyknife\Packages\Traits;

use Illuminate\Validation\ValidationException;

trait ResponseTrait
{

    protected $http_code = NULL;
    protected $http_message = NULL;

    /**
     * @param $data
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function responseData($data, $message = NULL)
    {
        $code  = 200;

        if(is_null($data)){
            $code = 204;
        }

        return response([
            'code' => $code,
            'message' => (is_null($message)) ? http_response($code) : $message,
            'payload' => $data,
            'settings' => [
                'actions' => $this->availableMethod,
                'fieldsType' => $this->fieldsType
            ]
        ]);
    }

    /**
     * @param \Exception $e
     * @param array $data
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function responseException(\Exception $e, $data = [])
    {
        return response(array_merge(
            $data,
            [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ]
        ),500);
    }

    /**
     * @param ValidationException $e
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function responseExceptionValidation(ValidationException $e)
    {
        return response([
                'code' => 422,
                'message' => http_response(422),
                'payload' => $e->errors()
        ],422);
    }
}
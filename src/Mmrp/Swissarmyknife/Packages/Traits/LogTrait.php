<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 09/04/18
 * Time: 10:58
 */

namespace Mmrp\Swissarmyknife\Packages\Traits;

use Illuminate\Http\Request;
use Mmrp\Swissarmyknife\Models\Log;

trait LogTrait
{
    public function log( $type, LogRequest $logRequest, Request $request, \Exception $e = NULL )
    {
        $log = new Log();
        $log->type = $type;
        $log->action = $logRequest->action;
        $log->resource = $logRequest->resource;
        $log->resource_id = $logRequest->resource_id;
        $log->code = ($e) ? $e->getCode() : 200 ;
        $log->message = ($e) ? $e->getMessage() : 'OK';
        $log->user_id = NULL;
        $log->request = json_encode($request);
        $log->file = (!$e) ?: $e->getFile();
        $log->line = (!$e) ?: $e->getLine();
        $log->trace = (!$e) ?: $e->getTraceAsString();

        $log->save();
    }
}

class LogRequest
{
    public $action;

    public $resource;

    public $resource_id;

}
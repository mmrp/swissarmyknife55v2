<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 16/04/18
 * Time: 09:45
 */

namespace Mmrp\Swissarmyknife\Packages\ExportData;

use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Mmrp\Swissarmyknife\Models\ExportData\ExportDataLog;
use Mmrp\Swissarmyknife\Models\Notification;

class ExportData
{
    private $model;
    private $type;

    private $storagePath;
    private $fileName;

    private $writer;

    private $withHeader = TRUE;
    private $header;

    private $exporDataLogId;

    /**
     * ExportData constructor.
     * @param Model $model
     * @param string $type
     * @param string $fileName
     */
    public function __construct(Model $model, $type, $fileName)
    {
        $this->model = $model;
        $this->type = $type;

        $this->storagePath = storage_path('app/export');

        $this->fileName = $this->storagePath . '/' . $fileName;

        $this->writer = $this->writerFactory();

        if($this->withHeader) {
            $this->header = $this->setHeader();
        }
    }

    public function exportData()
    {
        try {
            $extractedRows = 0;
            $rowsToExport = $this->model->count();
            $this->exporDataLogId = ExportDataLog::started($this->fileName, get_class($this->model));

            if (!File::isDirectory($this->storagePath)) {
                Storage::makeDirectory('export');
            }
            $this->writer->openToFile($this->fileName);

            if ($this->withHeader) {
                $this->writer->addRow($this->header);
            }

            foreach ($this->model->all() as $row) {
                $this->writer->addRow(array_values($row->toArray()));
                $extractedRows++;
                ExportDataLog::exporting($this->exporDataLogId, $extractedRows . '/' . $rowsToExport);
            }

            $this->writer->close();

            Notification::success('System', 'User', 'Export Completed', 'File ready', $this->fileName);
            ExportDataLog::ended($this->exporDataLogId);

            return [
                'inserted' => TRUE,
                'rows' => $extractedRows
            ];
        }
        catch (\Exception $e) {
            ExportDataLog::error($this->exporDataLogId, $e->getMessage());
            Notification::error('System', 'User', 'Export Failed', $e->getMessage());

            return [
                'exported' => FALSE,
                'rows' => 0,
                'error' => $e
            ];

        }
    }

    /**
     * @return array|\Exception
     */
    private function setHeader()
    {
        try {
            return array_keys($this->model->first()->toArray());
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @return \Box\Spout\Writer\WriterInterface|\Exception
     */
    private function writerFactory()
    {
        try {
            switch ($this->type) {
                case 'xlsx':
                    $writerType = Type::XLSX; // for XLSX files
                    break;
                case 'csv':
                    $writerType = Type::CSV; // for CSV files
                    break;
                case 'ods':
                    $writerType = Type::ODS; // for ODS files
                    break;
                default:
                    $writerType = NULL;

            }

            return WriterFactory::create($writerType);
        }
        catch (\Exception $e) {
            return $e;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 20/04/18
 * Time: 14:41
 */

namespace Mmrp\Swissarmyknife\Packages\File;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Upload
{
    private $inputFileForm;

    private $request;

    private $storagePath;

    /**
     * Upload constructor.
     * @param Request $request
     * @param string $inputFileForm
     * @param null $storagePath
     */
    public function __construct(Request $request, $inputFileForm = 'file' , $storagePath = NULL)
    {
        $this->inputFileForm = $inputFileForm;
        $this->request = $request;
        $this->storagePath = $this->setStoragePath($storagePath);
    }

    /**
     * @return \Exception|false|string
     */
    public function upload()
    {
        try {
            $file = $this->request->file($this->inputFileForm);

            $path = $file->storeAs(
                $this->storagePath,
                uniqid('upload_') . '.' . $file->getClientOriginalExtension()
            );

            return storage_path('app') . '/' . $path;
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param $storagePath
     * @return string
     */
    private function setStoragePath($storagePath)
    {
        $storagePath = ($storagePath) ? $storagePath :'uploads';

        return $storagePath;

    }
}
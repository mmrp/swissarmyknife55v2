<?php

namespace Mmrp\Swissarmyknife\Models\Rbac;

use Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model;

class Action extends Model
{
    protected $primaryKey = 'hash';

    protected $fillable = ['name'];

}

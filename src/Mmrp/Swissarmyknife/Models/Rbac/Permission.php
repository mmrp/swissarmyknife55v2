<?php

namespace Mmrp\Swissarmyknife\Models\Rbac;

use Illuminate\Database\Eloquent\SoftDeletes;
use Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model;

class Permission extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'model'
    ];

    public function roles()
    {
        return $this->belongsToMany('Mmrp\Swissarmyknife\Models\Rbac\Role');
    }

    public function users()
    {
        return $this->belongsToMany('Mmrp\Swissarmyknife\Models\Rbac\User');

    }
}

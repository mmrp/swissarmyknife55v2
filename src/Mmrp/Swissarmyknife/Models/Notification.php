<?php

namespace Mmrp\Swissarmyknife\Models;

use Carbon\Carbon;
use Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model;

class Notification extends Model
{
    protected $fillable = [
        'id',
        'type',
        'subject',
        'message',
        'from',
        'to',
        'download_path',
        'notify_at',
        'opened_at',
        'chrome_notification_at',
    ];

    public function userTo()
    {
        return $this->belongsTo('Mmrp\Swissarmyknife\Models\User','to','email');
    }

    public function userFrom()
    {
        return $this->belongsTo('Mmrp\Swissarmyknife\Models\User','from','email');
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function open($id)
    {
        return self::where('id', $id)->update([
            'opened_at' => Carbon::now()
        ]);
    }

    /**
     * @param $from
     * @param $to
     * @param $subject
     * @param $message
     * @param $downloadPath
     * @return mixed
     */
    public static function success($from, $to, $subject, $message, $downloadPath)
    {
        return self::add('success', $from, $to, $subject, $message, $downloadPath);
    }

    /**
     * @param $from
     * @param $to
     * @param $subject
     * @param $message
     * @param $downloadPath
     * @return mixed
     */
    public static function info($from, $to, $subject, $message, $downloadPath = NULL)
    {
        return self::add('info', $from, $to, $subject, $message, $downloadPath);
    }

    /**
     * @param $from
     * @param $to
     * @param $subject
     * @param $message
     * @return mixed
     */
    public static function error($from, $to, $subject, $message)
    {
        return self::add('error', $from, $to, $subject, $message);
    }

    /**
     * @param $type
     * @param $from
     * @param $to
     * @param $subject
     * @param $message
     * @param null $downloadPath
     * @return mixed
     */
    private static function add($type, $from, $to, $subject, $message, $downloadPath = NULL)
    {
        return self::create([
            'type' => $type,
            'subject' => $subject,
            'message' => $message,
            'from' => $from,
            'to' => $to,
            'download_path' => ($downloadPath) ? base64_encode($downloadPath) : NULL,
            'notify_at' => Carbon::now()
        ]);
    }
}

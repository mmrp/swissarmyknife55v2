<?php

namespace Mmrp\Swissarmyknife\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model;

class Log extends Model
{
    use SoftDeletes;

    public $fillable = [
        'type',
        'action',
        'resource',
        'resource_id',
        'code',
        'message',
        'user_id',
        'request',
        'session',
        'file',
        'line',
        'trace'
    ];

    public function user()
    {
        return $this->belongsTo('Mmrp\Swissarmyknife\Models\Rbac\User');
    }
}

<?php

namespace Mmrp\Swissarmyknife\Models\ExportData;

use Carbon\Carbon;
use Mmrp\Swissarmyknife\Packages\Database\Manipulation\Model;

class ExportDataLog extends Model
{
    protected $fillable = [
        'from',
        'file_path',
        'resource',
        'status',
        'progress',
        'started_at',
        'ended_at',
    ];

    /**
     * @param $filePath
     * @param $resource
     * @return mixed
     */
    public static function started($filePath, $resource)
    {
        $start = self::create([
            'from' => 'User logged email',
            'file_path' => base64_encode($filePath),
            'resource' => $resource,
            'status' => 'Started',
            'started_at' => Carbon::now()
        ]);

        return $start->id;
    }

    /**
     * @param $id
     * @param $progress
     * @return mixed
     */
    public static function exporting($id, $progress)
    {
        return self::where('id', $id)
            ->update([
                'status' => 'exporting',
                'progress' => $progress
            ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function ended($id)
    {
        return self::where('id', $id)
            ->update([
                'status' => 'ended',
                'ended_at' => Carbon::now()
            ]);
    }



    /**
     * @param $id
     * @param $error
     * @return mixed
     */
    public static function error($id, $error)
    {
        return self::where('id', $id)
            ->update([
                'status' => 'error',
                'progress' => $error
            ]);
    }
}

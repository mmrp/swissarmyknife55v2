<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 09/04/18
 * Time: 12:08
 */
namespace Mmrp\Swissarmyknife\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;
use Mmrp\Swissarmyknife\Http\Controllers\Traits\BeforeAction;
use Mmrp\Swissarmyknife\Http\Controllers\Traits\BeforeResponse;
use Mmrp\Swissarmyknife\Http\Controllers\Traits\BulkInsert;
use Mmrp\Swissarmyknife\Http\Controllers\Traits\ExportData;
use Mmrp\Swissarmyknife\Http\Controllers\Traits\File;
use Mmrp\Swissarmyknife\Models\Rbac\User;
use Mmrp\Swissarmyknife\Packages\Traits\LogRequest;
use Mmrp\Swissarmyknife\Packages\Traits\LogTrait;
use Mmrp\Swissarmyknife\Packages\Traits\ResponseTrait;

class CrudController extends Controller
{
    use BeforeAction;
    use BeforeResponse;
    use BulkInsert;
    use ExportData;
    use File;
    use LogTrait;
    use ResponseTrait;

    protected $request;

    protected $model;
    protected $primaryKey;
    protected $validationRules;

    protected $related;
    protected $fieldsType;

    protected $resource;
    protected $currentAction;
    protected $logRequest;

    protected $response;

    protected $fields;
    protected $query;
    protected $order;
    protected $page;
    protected $perPage;

    protected $checkPermissions = FALSE;
    protected $tokenField;

    protected $availableMethod = [
        'index' => FALSE,
        'get' => FALSE,
        'store' => FALSE,
        'update' => FALSE,
        'multipleUpdate' => FALSE,
        'delete' => FALSE,
        'destroy' => FALSE,
        'restore' => FALSE,
        'trash' => FALSE,

        'exportDataLog' => FALSE,
        'exportData' => FALSE,
        'DownloadExport' => FALSE,

        'bulkInsertLog' => FALSE,
        'bulkInsertPreview' => FALSE,
        'bulkInsert' => FALSE,

        'upload' => FALSE
    ];

    /**
     * CrudController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->currentAction = last(explode('@',Route::currentRouteAction()));

        $this->isAvailable();

        $this->primaryKey = $this->getPrimaryKey($request);

        $this->validationRules = $this->getValidationRules($request);

        $this->tokenField = env('API_KEY_TOKEN', 'ApiKey-Token');

        //MODEL MANIPULATION
        $this->fields = ['*'];
        $this->query = $request->input('q');
        $this->order = $request->input('o');
        $this->perPage = $request->input('perPage');
        $this->page = $request->input('page');

        //LOG REQUEST
        $this->logRequest = $this->makeLogRequest($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $this->hasPermission($request, 'viewAny');

        try {
            $this->model = $this->model->processQuery($this->fields, $this->query, $this->order);

            if($this->related) {
                $this->model = $this->model->with($this->related);
            }

            if($this->page or $this->perPage) {
                $this->model = $this->model->paginate($this->perPage);
            } else {
                $this->model = $this->model->get();
            }

            $this->response = $this->model;

            $this->beforeIndexResponse();

            $this->log('info', $this->logRequest, $request);

            return $this->responseData($this->response);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e, ['index' => false]);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function get(Request $request, $id)
    {
        $this->hasPermission($request, 'view');

        try {
            $this->query = [
                [ $this->primaryKey, '=', $id ]
            ];

            $this->model = $this->model->processQuery($this->fields, $this->query, $this->order);


            if($this->related) {
                $this->model = $this->model->with($this->related);
            }

            $this->model = $this->model->firstOrFail();


            $this->response = $this->model;
            $this->beforeGetResponse();

            $this->log('info', $this->logRequest, $request);

            return $this->responseData($this->response);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $this->hasPermission($request, 'create');

        try {
            $request->validate($this->validationRules);

            $this->save($request);

            $this->beforeStoreResponse();

            $this->log('info', $this->logRequest, $request);

            return $this->responseData([
                'inserted' => true,
                'data' => $this->model
            ]);
        }
        catch (ValidationException $e) {
            return $this->responseExceptionValidation($e);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $this->hasPermission($request, 'update');

        try {
            $this->model = $this->model->findOrFail($id);

            $this->save($request);

            $this->beforeUpdateResponse();

            $this->log('info', $this->logRequest, $request);

            return $this->responseData([
                'updated' => true,
                'data' => $this->model
            ]);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function multipleUpdate(Request $request)
    {
        $this->hasPermission($request, 'update');

        try {
            $ids = $request->input('rows_id');
            $data = $request->except('rows_id');

            $this->model = $this->model
                ->whereIn($this->primaryKey, $ids)
                ->update($data);

            $this->beforeMultipleUploadResponse();

            $this->log('info', $this->logRequest, $request);

            return $this->responseData([
                'updated' => ($this->model) ? TRUE : FALSE,
                'rows' => $this->model
            ]);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(Request $request, $id)
    {
        $this->hasPermission($request, 'delete');

        try {
            if($id == 'multiple' and $request->input('rows_id')) {
                $this->model = $this->model->whereIn('id', $request->input('rows_id'));
            } else {
                $this->model = $this->model->where('id',$id)->first();
            }

            if(!is_null($this->model)) {
                $this->model = $this->model->delete();
            }

            $this->beforeDeleteResponse();

            $this->log('info', $this->logRequest, $request);

            return $this->responseData([
                'deleted' => ($this->model) ? TRUE : FALSE,
                'rows' => ($this->model) ? (($id == 'multiple') ? $this->model : 1) : 0
            ]);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->hasPermission($request, 'forceDelete');

        try {
            $this->model = $this->model->onlyTrashed();

            if($id == 'multiple' and $request->input('rows_id')) {
                $this->model = $this->model->whereIn('id', $request->input('rows_id'));
            } else {
                $this->model = $this->model->where('id',$id)->first();
            }

            if(!is_null($this->model)) {
                $this->model = $this->model->forceDelete();
            }

            $this->beforeDestroyResponse();

            $this->log('info', $this->logRequest, $request);

            return $this->responseData([
                'destroyed' => ($this->model) ? TRUE : FALSE,
                'rows' => ($this->model) ? (($id == 'multiple') ? $this->model : 1) : 0
            ]);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function restore(Request $request, $id)
    {
        $this->hasPermission($request, 'restore');

        try {
            $this->model = $this->model->onlyTrashed();

            if($id == 'multiple' and $request->input('rows_id')) {
                $this->model = $this->model->whereIn('id', $request->input('rows_id'));
            } else {
                $this->model = $this->model->where('id',$id)->first();
            }

            if(!is_null($this->model)) {
                $this->model = $this->model->restore();
            }

            $this->beforeRestoreResponse();

            $this->log('info', $this->logRequest, $request);

            return $this->responseData([
                'restored' => ($this->model) ? TRUE : FALSE,
                'rows' => ($this->model) ? (($id == 'multiple') ? $this->model : 1) : 0
            ]);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function trash(Request $request)
    {
        $this->hasPermission($request, 'viewAny');

        try {
            $this->model = $this->model
                ->processQuery($this->fields, $this->query, $this->order)
                ->onlyTrashed();

            if($this->related) {
                $this->model = $this->model->with($this->related);
            }

            if($this->page or $this->perPage) {
                $this->model = $this->model->paginate($this->perPage);
            } else {
                $this->model = $this->model->get();
            }

            $this->response = $this->model;

            $this->log('info', $this->logRequest, $request);

            $this->beforeTrashResponse();

            return $this->responseData($this->response);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e);
        }

    }

    /**
     * @return null
     */
    protected function isAvailable()
    {
        if($this->request->server('SCRIPT_FILENAME') == 'artisan') {
            return NULL;
        }

        if( !in_array($this->currentAction, $this->availableMethod) or
            !isset($this->availableMethod[$this->currentAction]) or
            ( isset($this->availableMethod[$this->currentAction]) and $this->availableMethod[$this->currentAction] === FALSE )
        ) {
            abort(501);
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    protected function getPrimaryKey(Request $request)
    {
        return ($request->input('key')) ?: $this->model->getKeyName();
    }

    /**
     * @param Request $request
     */
    protected function save(Request $request)
    {
        foreach ($request->input() as $key => $value) {
            if(in_array($key, $this->model->getFillable())) {
                $this->model->$key = $value;
            }
        }

        $this->beforeSave();

        $this->model->save();
    }

    /**
     * @return LogRequest
     */
    protected function makeLogRequest()
    {
        if($this->request->server('SCRIPT_FILENAME') == 'artisan') {
            return NULL;
        }

        $logRequest = new LogRequest();
        $logRequest->action = $this->currentAction;
        $logRequest->resource = $this->resource;
        $logRequest->resource_id = $this->request->route()->parameter($this->primaryKey);

        return $logRequest;
    }

    /**
     * @param Request $request
     * @return array|null
     */
    protected function getValidationRules(Request $request)
    {
        if($this->request->server('SCRIPT_FILENAME') == 'artisan') {
            return NULL;
        }

        list($class, $method) = explode('@',str_replace(['Controllers','Controller'], ['Requests',''], Route::currentRouteAction()));

        $id = $request->route()->parameter('id');

        if(method_exists($class, $method)) {
            return ($class . '::' . $method)();
        }

        return [];
    }

    /**
     * @param Request $request
     * @param $action
     */
    private function hasPermission(Request $request, $action)
    {
        if($this->checkPermissions == FALSE) {
            return TRUE;
        }

        $token = $request->header($this->tokenField);

        $user = (new User())->getByToken($token);

        if (!$user->hasPermission('App/Models/Gamer@' . $action)) {
            abort(403);
        }
    }
}

<?php

namespace Mmrp\Swissarmyknife\Http\Controllers\Rbac;

use Mmrp\Swissarmyknife\Http\Controllers\CrudController;
use Mmrp\Swissarmyknife\Models\Rbac\Permission;
use Mmrp\Swissarmyknife\Models\Rbac\User;
use Illuminate\Http\Request;

class UserController extends CrudController
{
    protected $resource = 'RBAC\User';

    protected $related = [ 'roles', 'permissions' ];

    protected $availableMethod = [
        'index' => TRUE,
        'get' => TRUE,
        'store' => TRUE,
        'update' => TRUE,
        'multipleUpdate' => TRUE,
        'delete' => TRUE,
        'destroy' => TRUE,
        'restore' => TRUE,
        'trash' => TRUE,

        'exportDataLog' => TRUE,
        'exportData' => TRUE,
        'downloadExport' => TRUE,

        'bulkInsertLog' => TRUE,
        'bulkInsertPreview' => TRUE,
        'bulkInsert' => TRUE,

        'upload' => TRUE,

        'attachPermission' => TRUE,
        'detachPermission' => TRUE,
    ];

    public function __construct(Request $request)
    {
        $this->model = new User();
        $this->primaryKey = $this->getPrimaryKey($request);

        $this->fieldsType = [
            'name' => makeFieldInput($this->model, 'name'),
            'phone' => makeFieldInput($this->model, 'phone'),
            'email' => makeFieldInput($this->model, 'email'),
            'password' => makeFieldPassword($this->model, 'password'),
            'profile_image' => makeFieldProfileImage($this->model, 'profile_image'),
            'roles' => makeFieldRelationship($this->model, 'roles', 'role_id', 'name', 'many-to-many')
        ];

        parent::__construct($request);
    }

    protected function beforeGetResponse()
    {
        list($this->response->permissionsMatrix, $this->response->actionsList) = $this->permissions();
    }

    public function attachPermission(Request $request, $id, $permission_id)
    {
        try{
            $this->model = $this->model->findOrFail($id);

            $this->model->attachPermissions($permission_id);

            return $this->responseData(['permission' => 'attached']);
        }
        catch (\Exception $e){
            return $this->responseException($e);
        }
    }

    public function detachPermission(Request $request, $id, $permission_id)
    {
        try{
            $this->model = $this->model->findOrFail($id);

            $this->model->detachPermissions($permission_id);

            return $this->responseData(['permission' => 'detached']);

        }
        catch (\Exception $e){
            return $this->responseException($e);
        }
    }

    private function permissions()
    {
        $role_permissions = $this->model->allPermissions();

        $permission_matrix = [];
        $actions = [];

        foreach (Permission::all(['id', 'name'])->toArray() as $permission) {
            list($controller, $action) = explode('@', $permission['name']);

            if (!isset($actions[$action])) {
                $actions[$action] = TRUE;
            }

            if (!isset($permission_matrix[$controller])) {
                $permission_matrix[$controller] = [];
            }

            $permission_matrix[$controller][$action] = [
                'id' => $permission['id'],
                'enabled' => (isset($role_permissions[$controller . '@' . $action]) and $role_permissions[$controller . '@' . $action]) ? TRUE : FALSE,
            ];
        }

        return [
            $permission_matrix,
            $actions
        ];
    }
}

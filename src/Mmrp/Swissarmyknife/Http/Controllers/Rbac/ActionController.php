<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 10/04/18
 * Time: 10:53
 */

namespace Mmrp\Swissarmyknife\Http\Controllers\Rbac;

use Mmrp\Swissarmyknife\Http\Controllers\CrudController;
use Mmrp\Swissarmyknife\Models\Rbac\Action;
use Illuminate\Http\Request;

class ActionController extends CrudController
{
    protected $resource = 'RBAC\Action';

    protected $availableMethod = [
        'index' => TRUE,
    ];

    public function __construct(Request $request)
    {
        $this->model = new Action();
        $this->primaryKey = $this->getPrimaryKey($request);

        parent::__construct($request);
    }
}
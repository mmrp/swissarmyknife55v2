<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 10/04/18
 * Time: 09:43
 */

namespace Mmrp\Swissarmyknife\Http\Controllers\Rbac;

use Mmrp\Swissarmyknife\Http\Controllers\CrudController;
use Mmrp\Swissarmyknife\Models\Rbac\Permission;
use Mmrp\Swissarmyknife\Models\Rbac\Role;
use Mmrp\Swissarmyknife\Models\Rbac\User;
use Illuminate\Http\Request;

class RoleController extends CrudController
{
    protected $resource = 'RBAC\Role';

    protected $related = [ 'users', 'permissions', 'parent' ];

    protected $availableMethod = [
        'index' => TRUE,
        'get' => TRUE,
        'store' => TRUE,
        'update' => TRUE,
        'multipleUpdate' => TRUE,
        'delete' => TRUE,
        'destroy' => TRUE,
        'restore' => TRUE,
        'trash' => TRUE,

        'exportDataLog' => TRUE,
        'exportData' => TRUE,
        'downloadExport' => TRUE,

        'bulkInsertLog' => TRUE,
        'bulkInsertPreview' => TRUE,
        'bulkInsert' => TRUE,

        'upload' => TRUE,

        'attachPermission' => TRUE,
        'detachPermission' => TRUE,

        'attachUser' => TRUE,
        'detachUser' => TRUE,
    ];

    public function __construct(Request $request)
    {
        $this->model = new Role();
        $this->primaryKey = $this->getPrimaryKey($request);

        $this->fieldsType = [
            'name' => makeFieldInput($this->model, 'name'),
            'slug' => makeFieldHidden($this->model, 'slug'),
            'description' => makeFieldInput($this->model, 'description'),
            'parent_id' => makeFieldInput($this->model, 'parent_id'),
            'users' => makeFieldRelationship($this->model, 'users', 'user_id', 'name', 'one-to-many'),
            'permissions' => makeFieldRelationship($this->model, 'permissions', 'permission_id', 'name', 'one-to-many'),
        ];

        parent::__construct($request);
    }

    protected function beforeGetResponse()
    {
        list($this->response->permissionMatrix, $this->response->actionList) = $this->permissions();
        $this->response->usersList = $this->users();
    }

    protected function beforeSave()
    {
        $this->model->slug = slug($this->model->name);
    }

    public function attachPermission(Request $request, $id, $permission_id)
    {
        try{
            $this->model = $this->model->findOrFail($id);

            $this->model->attachPermissions($permission_id);

            return $this->responseData(['permission' => 'attached']);;
        }
        catch (\Exception $e){
            return $this->responseException($e);
        }
    }

    public function detachPermission(Request $request, $id, $permission_id)
    {
        try{
            $this->model = $this->model->findOrFail($id);

            $this->model->detachPermissions($permission_id);

            return $this->responseData(['permission' => 'detached']);
        }
        catch (\Exception $e){
            return $this->responseException($e);
        }
    }

    public function attachUser(Request $request, $id, $user_id)
    {
        try{
            $this->model = $this->model->findOrFail($id);

            $this->model->attachUsers($user_id);

            return $this->responseData(['user' => 'attached']);
        }
        catch (\Exception $e){
            return $this->responseException($e);
        }
    }

    public function detachUser(Request $request, $id, $user_id)
    {
        try{
            $this->model = $this->model->findOrFail($id);

            $this->model->detachUsers($user_id);

            return $this->responseData(['user' => 'detached']);
        }
        catch (\Exception $e){
            return $this->responseException($e);
        }
    }

    private function permissions()
    {
        $role_permissions = [];
        $permission_matrix = [];
        $actions = [];

        foreach ($this->model->permissions->pluck('name') as $permission) {
            if (!isset($role_permissions[$permission])) {
                $role_permissions[$permission] = true;
            }
        }

        foreach (Permission::all(['id', 'name'])->toArray() as $permission) {
            list($controller, $action) = explode('@', $permission['name']);

            if (!isset($actions[$action])) {
                $actions[$action] = TRUE;
            }

            if (!isset($permission_matrix[$controller])) {
                $permission_matrix[$controller] = [];
            }

            $permission_matrix[$controller][$action] = [
                'id' => $permission['id'],
                'enabled' => isset($role_permissions[$controller . '@' . $action]) ? TRUE : FALSE,
            ];
        }

        return [
            $permission_matrix,
            $actions
        ];
    }

    private function users()
    {
        $users = [];
        $users_role = [];

        foreach ($this->model->users->pluck('email')->toArray() as $i => $item) {
            $users_role[$item] = TRUE;
        }


        foreach (User::all('id','name','email') as $item) {
            $users[$item->email] = [
                'enabled' => (isset($users_role[$item->email]) ? TRUE : FALSE),
                'id' => $item->id,
                'name' => $item->name,
                'email' => $item->email,
            ];
        }

        return $users;
    }
}
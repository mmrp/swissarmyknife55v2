<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 10/04/18
 * Time: 09:43
 */

namespace Mmrp\Swissarmyknife\Http\Controllers\Rbac;

use Mmrp\Swissarmyknife\Http\Controllers\CrudController;
use Mmrp\Swissarmyknife\Models\Rbac\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class PermissionController extends CrudController
{
    protected $resource = 'RBAC\Permission';

    protected $related = [ 'roles', 'users' ];

    protected $availableMethod = [
        'index' => TRUE,
        'get' => TRUE,
        'store' => TRUE,
        'update' => TRUE,
        'multipleUpdate' => TRUE,
        'delete' => TRUE,
        'destroy' => TRUE,
        'restore' => TRUE,
        'trash' => TRUE,

        'exportDataLog' => TRUE,
        'exportData' => TRUE,
        'downloadExport' => TRUE,

        'bulkInsertLog' => TRUE,
        'bulkInsertPreview' => TRUE,
        'bulkInsert' => TRUE,

        'upload' => TRUE,

        'updateActions' => TRUE
    ];

    public function __construct(Request $request)
    {
        $this->model = new Permission();
        $this->primaryKey = $this->getPrimaryKey($request);

        $this->fieldsType = [
            'name' => makeFieldInput($this->model, 'name'),
            'slug' => makeFieldHidden($this->model, 'slug'),
            'description' => makeFieldInput($this->model, 'description'),
            'model' => makeFieldInput($this->model, 'model'),
            'roles' => makeFieldRelationship($this->model, 'roles', 'role_id',' name', 'one-to-many'),
            'users' => makeFieldRelationship($this->model, 'users', 'user_id', 'name', 'one-to-many')
        ];

        parent::__construct($request);
    }

    protected function beforeSave()
    {
        $this->model->slug = slug($this->model->name);
    }

    public function updateActions(Request $request)
    {
        try {
            Artisan::call('rbac:actions');

            return $this->responseData([]);
        }
        catch (\Exception $e) {
            return $this->responseException($e);
        }
    }
}
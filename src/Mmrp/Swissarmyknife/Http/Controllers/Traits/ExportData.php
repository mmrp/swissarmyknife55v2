<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 17/04/18
 * Time: 09:50
 */

namespace Mmrp\Swissarmyknife\Http\Controllers\Traits;

use Mmrp\Swissarmyknife\Models\ExportData\ExportDataLog;
use Illuminate\Http\Request;

trait ExportData
{
    protected $exportDataModelLog;
    protected $exportData;

    /**
     * @param Request $request
     * @return mixed
     */
    public function exportDataLog(Request $request)
    {
        try {
            $this->exportDataModelLog = new ExportDataLog();

            $this->exportDataModelLog = $this->exportDataModelLog->processQuery($this->fields, $this->query, $this->order);

            $this->exportDataModelLog = $this->exportDataModelLog->where('resource', get_class($this->model));

            if($this->page or $this->perPage) {
                $this->exportDataModelLog = $this->exportDataModelLog->paginate($this->perPage);
            } else {
                $this->exportDataModelLog = $this->exportDataModelLog->get();
            }

            $this->log('info', $this->logRequest, $request);

            return $this->responseData($this->exportDataModelLog);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e, ['index' => false]);
        }
    }

    /**
     * @param Request $request
     * @param $type
     * @return mixed
     */
    public function exportData(Request $request, $type)
    {
        try {
            $this->model = $this->model->processQuery($this->fields, $this->query, $this->order);
            $this->exportData = new \Mmrp\Swissarmyknife\Packages\ExportData\ExportData($this->model, $type, uniqid() . '.xlsx');

            $this->exportData->exportData();

            $this->log('info', $this->logRequest, $request);

            return $this->responseData([
                'export' => 'started'
            ]);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e);
        }
    }

    /**
     * @param Request $request
     * @param $filePath
     * @return mixed
     */
    public function downloadExport(Request $request, $filePath)
    {
        try {
            $filePath = base64_decode($filePath);
            return response()->download($filePath);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e);
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 20/04/18
 * Time: 15:12
 */

namespace Mmrp\Swissarmyknife\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Mmrp\Swissarmyknife\Packages\File\Upload;

trait File
{
    protected $fileUpload;

    /**
     * @param Request $request
     * @return mixed
     */
    public function upload(Request $request)
    {
        try {
            $this->fileUpload = new Upload($request);

            $path = $this->fileUpload->upload();

            $this->log('info', $this->logRequest, $request);

            return $this->responseData([
                'filePath' => base64_encode($path)
            ]);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e, ['index' => false]);
        }
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 17/04/18
 * Time: 09:48
 */
namespace Mmrp\Swissarmyknife\Http\Controllers\Traits;

trait BeforeResponse
{
    protected function beforeIndexResponse()
    {
        //TODO: put here the code to be executed before the indexResponse
    }

    protected function beforeGetResponse()
    {
        //TODO: put here the code to be executed before the getResponse
    }

    protected function beforeStoreResponse()
    {
        //TODO: put here the code to be executed before the storeResponse
    }

    protected function beforeUpdateResponse()
    {
        //TODO: put here the code to be executed before the updateResponse
    }

    protected function beforeMultipleUploadResponse()
    {
        //TODO: put here the code to be executed before the multipleUploadResponse
    }

    protected function beforeDeleteResponse()
    {
        //TODO: put here the code to be executed before the deleteResponse
    }

    protected function beforeDestroyResponse()
    {
        //TODO: put here the code to be executed before the destroyResponse
    }

    protected function beforeRestoreResponse()
    {
        //TODO: put here the code to be executed before the restoreResponse
    }

    protected function beforeTrashResponse()
    {
        //TODO: put here the code to be executed before the trashResponse
    }
}
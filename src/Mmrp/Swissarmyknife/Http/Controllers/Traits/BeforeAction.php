<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 18/04/18
 * Time: 16:19
 */

namespace Mmrp\Swissarmyknife\Http\Controllers\Traits;


trait BeforeAction
{
    protected function beforeSave()
    {
        //TODO: put here the code to be executed before saveMethod
    }
}
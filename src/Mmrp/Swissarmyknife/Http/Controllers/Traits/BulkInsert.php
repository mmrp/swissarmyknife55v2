<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 17/04/18
 * Time: 09:51
 */

namespace Mmrp\Swissarmyknife\Http\Controllers\Traits;

use Mmrp\Swissarmyknife\Models\BulkInsert\BulkInsertLog;
use Illuminate\Http\Request;

trait BulkInsert
{
    protected $bulkInsertModelLog;
    protected $bulkInsert;

    /**
     * @param Request $request
     * @return mixed
     */
    public function bulkInsertLog(Request $request)
    {
        try {
            $this->bulkInsertModelLog = new BulkInsertLog();

            $this->bulkInsertModelLog = $this->bulkInsertModelLog->processQuery($this->fields, $this->query, $this->order);

            $this->bulkInsertModelLog = $this->bulkInsertModelLog->where('resource', get_class($this->model));

            if($this->page or $this->perPage) {
                $this->bulkInsertModelLog = $this->bulkInsertModelLog->paginate($this->perPage);
            } else {
                $this->bulkInsertModelLog = $this->bulkInsertModelLog->get();
            }

            $this->log('info', $this->logRequest, $request);

            return $this->responseData($this->bulkInsertModelLog);
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e, ['index' => false]);
        }
    }

    /**
     * @param Request $request
     * @param $filePath
     * @return mixed
     */
    public function bulkInsertPreview(Request $request, $filePath)
    {
        try {
            $this->bulkInsert = $this->bulkInsertFactory($request, $filePath);

            $this->log('info', $this->logRequest, $request);

            $this->bulkInsert->preview();

            return $this->responseData([
                'header' => $this->bulkInsert->getHeader(),
                'body' => $this->bulkInsert->getBody()
            ]);

        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e, ['bulkInsertPreview' => false]);
        }
    }

    /**
     * @param Request $request
     * @param $filePath
     * @return mixed
     */
    public function bulkInsert(Request $request, $filePath)
    {
        try {
            $this->bulkInsert = $this->bulkInsertFactory($request, $filePath);

            if($request->input('mapping')) {
                $this->bulkInsert->setMapping($request->input('mapping'));
//                $bulkInsert->setMapping(
//                    [
//                        'name' => [ '{{ nome }}', ' ', '{{ cognome }}'],
//                        'phone' => [ '{{ telefono_mobile }}', ' ', '{{ telefono_fisso }}' ],
//                        'email' => [ '{{ nome }}', '.', '{{ cognome }}', '@gmail.com' ],
//                        'password' => [ '(( encrypt(str_random(16)) ))' ],
//                        'password' => [ '(( App\Models\Rbac\User::findOrFail(1)->email ))' ],
//                        'password' => [ '(( getHtmlPage(\'{{ nome }}\', \'{{ cognome }}\') ))' ],
//                    ]
//                );
            }

            $this->bulkInsert->bulkInsert($this->model, TRUE);

        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e, ['bulkInsertPreview' => false]);
        }
    }


    /**
     * @param Request $request
     * @param $filePath
     * @return \Mmrp\Swissarmyknife\Packages\BulkInsert\BulkInsert
     */
    private function bulkInsertFactory(Request $request, $filePath)
    {
        try {
            $filePath = base64_decode($filePath);
            $filePath = '/home/vagrant/Code/laravel55/storage/app/LEADS_DUPLICATED.xlsx';

            $this->bulkInsert = new \Mmrp\Swissarmyknife\Packages\BulkInsert\BulkInsert($filePath);

            return $this->bulkInsert;
        }
        catch (\Exception $e) {
            $this->log('error', $this->logRequest, $request, $e);

            return $this->responseException($e, ['bulkInsertPreview' => false]);
        }

    }
}
<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 10/04/18
 * Time: 10:24
 */

namespace Mmrp\Swissarmyknife\Http\Requests\Rbac;

use Illuminate\Validation\Rule;

class Permission
{
    public static function store()
    {
        return [
            'name' => ['required', 'string', Rule::unique('permissions')]
        ];
    }

    public static function update($id)
    {
        return [
            'name' => ['required', 'string', Rule::unique('permissions')->ignore($id)]
        ];
    }

    public static function multipleUpdate()
    {
        return [
            'rows_id' => [ 'required', 'array' ]
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 09/04/18
 * Time: 15:30
 */

namespace Mmrp\Swissarmyknife\Http\Requests\Rbac;

use Illuminate\Validation\Rule;

class User
{
    public static function store()
    {
        return [
            'name' => [ 'required', 'string' ],
            'phone' => [ 'regex:/(^[0]\d{6,10}$)|(^[3]\d{8,9}$)/' ],
            'email' => [ 'required', 'email', Rule::unique('users') ],
            'password' => [ 'required', 'confirmed' ],
            'profile_image' => [ 'image' ]
        ];
    }

    public static function update($id)
    {
        return [
            'name' => [ 'string' ],
            'phone' => [ 'regex:/(^[0]\d{6,10}$)|(^[3]\d{8,9}$)/' ],
            'email' => [ 'email', Rule::unique('users')->ignore($id) ],
            'password' => [ 'confirmed' ],
            'profile_image' => [ 'image' ]
        ];
    }

    public static function multipleUpdate()
    {
        return [
            'rows_id' => [ 'required', 'array' ]
        ];
    }
}
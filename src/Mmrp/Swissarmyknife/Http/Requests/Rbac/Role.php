<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 10/04/18
 * Time: 10:24
 */

namespace Mmrp\Swissarmyknife\Http\Requests\Rbac;

use Illuminate\Validation\Rule;

class Role
{
    public static function store()
    {
        return [
            'name' => ['required', 'string', Rule::unique('roles')]
        ];
    }

    public static function update($id)
    {
        return [
            'name' => ['required', 'string', Rule::unique('roles')->ignore($id)]
        ];
    }

    public static function multipleUpdate()
    {
        return [
            'rows_id' => [ 'required', 'array' ]
        ];
    }
}
<?php

namespace Mmrp\Swissarmyknife\Http\Middleware;

use Closure;
use Mmrp\Swissarmyknife\Models\Rbac\User;

class AuthenticationToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header(env('API_KEY_TOKEN', 'ApiKey-Token'));

        if(empty($token)) {
            return $this->tokenMissingResponse();
        }

        $user = (new User())->getByToken($token);

        if(empty($user)) {
            return $this->tokenInvalidResponse();
        }

        return $next($request);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    private function tokenMissingResponse()
    {
        return response()->json([
            'code' => 403,
            'message' => 'EFFE-ApiKey-Token Missing',
            'data' => NULL
        ], 403);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    private function tokenInvalidResponse()
    {
        return response()->json([
            'code' => 403,
            'message' => http_response(403),
            'data' => NULL
        ], 403);
    }
}

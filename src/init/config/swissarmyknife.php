<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 19/04/18
 * Time: 14:10
 */

return [
    'sms_default_provider' => env('SMS_DEFAULT_PROVIDER', NULL),

    /*
    |--------------------------------------------------------------------------
    | BitLy API Key
    |--------------------------------------------------------------------------
    */
    'bitly_token' =>  env('BITLY_TOKEN', NULL),

    /*
    |--------------------------------------------------------------------------
    | Amazon Web Service
    |--------------------------------------------------------------------------
    */
    'aws_sns_region' => env('AWS_SNS_REGION', NULL),
    'aws_sns_secret' => env('AWS_SNS_SECRET', NULL),
    'aws_sns_key' => env('AWS_SNS_KEY', NULL),

    /*
    |--------------------------------------------------------------------------
    | TIM SMS PRO
    |--------------------------------------------------------------------------
    */
    'tim_sms_pro_username' => env('TIM_SMS_PRO_USERNAME', NULL),
    'tim_sms_pro_password' => env('TIM_SMS_PRO_PASSWORD', NULL),
    'tim_sms_pro_token' => env('TIM_SMS_PRO_TOKEN', NULL),

    /*
    |--------------------------------------------------------------------------
    | MAIL UP
    |--------------------------------------------------------------------------
    */
    'swissarmyknife.mailup_username' => env('MAILUP_USERNAME' ,NULL),
    'swissarmyknife.mailup_password' => env('MAILUP_PASSWORD' ,NULL),
    'swissarmyknife.mailup_list_guid' => env('MAILUP_LIST_GUID' ,NULL),
    'swissarmyknife.mailup_list_secret' => env('MAILUP_LIST_SECRET' ,NULL),
    'swissarmyknife.mailup_campaign_code' => env('MAILUP_CAMPAIGN_CODE' ,NULL),
    'swissarmyknife.mailup_dynamic_fields' => env('MAILUP_DYNAMIC_FIELDS' ,NULL),
    'swissarmyknife.mailup_is_unicode' => env('MAILUP_IS_UNICODE' ,NULL),
];
<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function() {
    Route::group(['namespace' => 'Rbac', 'prefix' => 'rbac'], function () {
        Route::group(['prefix' => 'users'], function () {
            Route::get('{id}/permission/{permission_id}/attach', 'UserController@attachPermission');
            Route::get('{id}/permission/{permission_id}/detach', 'UserController@detachPermission');

            createCrudRoute('UserController');
        });

        Route::group(['prefix' => 'roles'], function () {
            Route::get('{id}/permission/{permission_id}/attach', 'RoleController@attachPermission');
            Route::get('{id}/permission/{permission_id}/detach', 'RoleController@detachPermission');
            Route::get('{id}/user/{user_id}/attach', 'RoleController@attachUser');
            Route::get('{id}/user/{user_id}/detach', 'RoleController@detachUser');

            createCrudRoute('RoleController');
        });

        Route::group(['prefix' => 'permissions'], function () {
            Route::get('update/actions', 'PermissionController@updateActions');
            createCrudRoute('PermissionController');
        });

        Route::group(['prefix' => 'actions'], function () {
            createCrudRoute('ActionController');
        });
    });
});
